﻿using System;
using System.Linq;
using System.Text;
using Org.BouncyCastle.Crypto.Generators;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Pkcs;
using Org.BouncyCastle.X509;
using Org.BouncyCastle.Asn1.X509;
using Org.BouncyCastle.Math;
using System.IO;
using Org.BouncyCastle.OpenSsl;
using Org.BouncyCastle.Crypto.Digests;
using Org.BouncyCastle.Crypto.Parameters;

namespace KeyGenerator
{
    public class Program
    {
        private static RsaPrivateCrtKeyParameters rootCAPrivate;
        private static SecureRandom random;

        public static void Main(string[] args)
        {
            string nameAndSurname;
            string email = "";
            string pass = "";
            bool ok = false;
            string next = "y";

            ReadRootCA();
            random = new SecureRandom();

            while (next != "n")
            {
                Console.WriteLine("Podaj imię i nazwisko:");
                nameAndSurname = Console.ReadLine();

                ok = false;
                Console.WriteLine("Podaj email:");
                while (!ok)
                {
                    email = Console.ReadLine();
                    if (email.Contains('@'))
                        ok = true;
                    else Console.WriteLine("To nie jest adres email...");
                }

                ok = false;
                Console.WriteLine("Podaj hasło:");
                while (!ok)
                {
                    pass = Console.ReadLine();
                    if (pass.Length >= 4)
                        ok = true;
                    else Console.WriteLine("Przynajmniej 4 znaki!");
                }

                AsymmetricCipherKeyPair rsaKeyPair;
                string pub = CreateFileName(nameAndSurname, false);
                string priv = CreateFileName(nameAndSurname, true);
                X509Certificate cert = GenerateCertificate(nameAndSurname, email, pass, out rsaKeyPair);
                SaveToFile(cert, pub);
                CloneFile(pub, priv);
                AppendPrivateKey(priv, EncodeKeyWithPass(rsaKeyPair, pass));

                Console.Write("Jeszcze jeden? (t/n): ");
                next = Console.ReadLine();
            }
        }

        private static void ReadRootCA()
        {
            using (StreamReader fs = new StreamReader("rootca.key", Encoding.UTF8))
            {
                PemReader reader = new PemReader(fs, new Password("rootca"));
                rootCAPrivate = (RsaPrivateCrtKeyParameters)reader.ReadObject();
            }
        }

        private static X509Certificate GenerateCertificate(string nameAndSurname, string emailString, string password, out AsymmetricCipherKeyPair keyPair)
        {
            RsaKeyPairGenerator g = new RsaKeyPairGenerator();
            g.Init(new KeyGenerationParameters(new SecureRandom(), 2048));
            keyPair = g.GenerateKeyPair(); 
            
            var gen = new X509V3CertificateGenerator();
            var subjectName = new X509Name("CN=" + nameAndSurname);
            var issuerName = new X509Name("CN=MyRootCA");
            var serialNo = BigInteger.ProbablePrime(120, new Random());
            gen.SetSerialNumber(serialNo);
            gen.SetSubjectDN(subjectName);
            gen.SetIssuerDN(issuerName);
            gen.SetNotAfter(DateTime.Now.AddYears(10));
            gen.SetNotBefore(DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)));
            gen.SetSignatureAlgorithm("SHA256withRSA");
            gen.SetPublicKey(keyPair.Public);
            gen.AddExtension("1.2.840.113549.1.9.1", false, Encoding.UTF8.GetBytes(emailString));
            var newCert = gen.Generate(rootCAPrivate);
            return newCert;
        }



        private static void SaveToFile(X509Certificate newCert, string filePath)
        {
            byte[] certBytes;
            using (StreamWriter certFile = new StreamWriter(filePath, false))
            {
                PemWriter writer = new PemWriter(certFile);
                writer.WriteObject(newCert);
                writer.Writer.Flush();
                // with FileStream:
                //certBytes = newCert.GetEncoded();
                //certFile.Write(certBytes, 0, certBytes.Length);
            }
        }

        private static byte[] GenerateTigerHash(string password)
        {
            TigerDigest tiger = new TigerDigest();
            byte[] hash = new byte[tiger.GetDigestSize()];
            byte[] passwordBytes = Encoding.UTF8.GetBytes(password);
            tiger.BlockUpdate(passwordBytes, 0, passwordBytes.Length);
            tiger.DoFinal(hash, 0);
            return hash;
        }

        private static byte[] EncodeKeyWithPass(AsymmetricCipherKeyPair keyPair, string password)
        {
            IBufferedCipher bufferedCipher;
            ICipherParameters cipherParameters;
            byte[] key = GenerateTigerHash(password);
            byte[] iv = new byte[8];
            byte[] outputBuffer;
            int offset = 0;

            random.NextBytes(iv, 0, 8);

            bufferedCipher = CipherUtilities.GetCipher("DESede/ECB/PKCS7Padding");

            KeyParameter keyParameter = ParameterUtilities.CreateKeyParameter("DESede", key);
            cipherParameters = keyParameter;
            bufferedCipher.Init(true, cipherParameters);

            PrivateKeyInfo privateKeyInfo = PrivateKeyInfoFactory.CreatePrivateKeyInfo(keyPair.Private);
            byte[] serializedPrivateBytes = privateKeyInfo.PrivateKey.GetDerEncoded();

            outputBuffer = new byte[bufferedCipher.GetOutputSize(serializedPrivateBytes.Length)];

            offset = bufferedCipher.ProcessBytes(serializedPrivateBytes, outputBuffer, 0);
            bufferedCipher.DoFinal(outputBuffer, offset);

            return outputBuffer;
        }

        private static void CloneFile(string originalPath, string newPath)
        {
            File.Copy(originalPath, newPath, true);
        }

        private static void AppendPrivateKey(string fileName, byte[] encryptedPrivateKey)
        {
            using (StreamWriter sw = new StreamWriter(fileName, true, Encoding.UTF8))
            {
                sw.WriteLine("-----BEGIN DES3ECB TIGER ENCRYPTED RSA PRIVATE KEY-----");
                sw.Write(Convert.ToBase64String(encryptedPrivateKey));
                sw.WriteLine();
                sw.WriteLine("-----END DES3ECB TIGER ENCRYPTED RSA PRIVATE KEY-----");
            }
        }

        private static string CreateFileName(string name, bool isPrivate)
        {
            string appended = isPrivate ? "priv" : "pub";
            string[] nameAndSurname = name.Split(' ');
            return nameAndSurname[0] + "_" + appended + ".pem";
        }


    }

    public class Password : IPasswordFinder
    {
        private string password;

        public Password(string password)
        {
            this.password = password;
        }

        public char[] GetPassword()
        {
            return password.ToCharArray();
        }
    }
}
