﻿using System.ComponentModel;

namespace CommonControls
{
    public abstract class PropertyChangedNotifier : INotifyPropertyChanged
    {
        #region Events

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion // end of Events

        #region Events Invocator

        protected void RaisePropertyChanged(string propertyName)
        {
            PropertyChangedEventHandler propertyChanged = PropertyChanged;
            if ((propertyChanged != null))
                propertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion // end of Events Invocator
    }
}