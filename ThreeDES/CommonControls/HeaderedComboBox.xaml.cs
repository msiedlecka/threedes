﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace CommonControls
{
    public partial class HeaderedComboBox : UserControl
    {
        public HeaderedComboBox()
        {
            InitializeComponent();
        }

        #region Dependency Properties

        #region Label

        public static DependencyProperty HeaderTextProperty =
            DependencyProperty.Register("HeaderText", typeof(string), typeof(HeaderedComboBox));

        public string HeaderText
        {
            get { return (string) GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }

        #endregion // end of Label

        #region ComboBox

        public static DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(IEnumerable), typeof(HeaderedComboBox));

        public IEnumerable Items
        {
            get { return (string) GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        public static DependencyProperty SelectedItemProperty =
            DependencyProperty.Register("SelectedItem", typeof(object), typeof(HeaderedComboBox));

        public object SelectedItem
        {
            get { return (string) GetValue(SelectedItemProperty); }
            set { SetValue(SelectedItemProperty, value); }
        }

        #endregion // end of ComboBox

        #endregion // end of Dependency Properties
    }
}