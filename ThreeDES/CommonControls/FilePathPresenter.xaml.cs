﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

namespace CommonControls
{
    public partial class FilePathPresenter : UserControl
    {
        public FilePathPresenter()
        {
            InitializeComponent();
        }

        #region Dependency Properties

        #region Button

        public static DependencyProperty ButtonImageSourceProperty =
            DependencyProperty.Register("ButtonImageSource", typeof(ImageSource), typeof(FilePathPresenter));

        public ImageSource ButtonImageSource
        {
            get { return (ImageSource) GetValue(ButtonImageSourceProperty); }
            set { SetValue(ButtonImageSourceProperty, value); }
        }

        public static DependencyProperty ButtonCommandProperty =
            DependencyProperty.Register("ButtonCommand", typeof(ICommand), typeof(FilePathPresenter));

        public ICommand ButtonCommand
        {
            get { return (ICommand) GetValue(ButtonCommandProperty); }
            set { SetValue(ButtonCommandProperty, value); }
        }

        public static DependencyProperty ButtonCommandParameterProperty =
            DependencyProperty.Register("ButtonCommandParameter", typeof(object), typeof(FilePathPresenter));

        public object ButtonCommandParameter
        {
            get { return GetValue(ButtonCommandParameterProperty); }
            set { SetValue(ButtonCommandParameterProperty, value); }
        }

        public static DependencyProperty ButtonToolTipProperty =
            DependencyProperty.Register("ButtonToolTip", typeof(string), typeof(FilePathPresenter));

        public string ButtonToolTip
        {
            get { return (string) GetValue(ButtonToolTipProperty); }
            set { SetValue(ButtonToolTipProperty, value); }
        }

        #endregion // end of Button

        #region TextBox

        public static DependencyProperty PathTextProperty =
            DependencyProperty.Register("PathText", typeof(string), typeof(FilePathPresenter));

        public string PathText
        {
            get { return (string) GetValue(PathTextProperty); }
            set { SetValue(PathTextProperty, value); }
        }

        #endregion // end of TextBox

        #region Label

        public static DependencyProperty HeaderTextProperty =
            DependencyProperty.Register("HeaderText", typeof(string), typeof(FilePathPresenter));

        public string HeaderText
        {
            get { return (string) GetValue(HeaderTextProperty); }
            set { SetValue(HeaderTextProperty, value); }
        }

        #endregion // end of Label

        #endregion // end of  Dependency Properties
    }
}