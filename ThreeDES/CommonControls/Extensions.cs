﻿using System.Windows;
using System.Windows.Media;

namespace ExtensionMethods
{
    public static class Extensions
    {
        public static T GetFirstRecursiveChildOfType<T>(this DependencyObject depObj) where T : DependencyObject
        {
            if (depObj == null) return null;

            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
            {
                var child = VisualTreeHelper.GetChild(depObj, i);

                var result = (child as T) ?? GetFirstRecursiveChildOfType<T>(child);
                if (result != null) return result;
            }
            return null;
        }
    }
}