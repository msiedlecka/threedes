﻿namespace CommonControls
{
    public interface ILabeled
    {
        string Label { get; }
    }
}