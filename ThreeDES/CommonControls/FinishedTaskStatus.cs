﻿namespace CommonControls
{
    public enum FinishedTaskStatus
    {
        SUCCESS,
        CANCELLED,
        FAILURE
    }
}