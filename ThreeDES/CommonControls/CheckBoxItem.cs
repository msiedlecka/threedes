﻿namespace CommonControls
{
    public class CheckBoxItem<T> : PropertyChangedNotifier where T : ILabeled
    {
        private bool isChecked;
        private T item;

        public CheckBoxItem(T obj, bool isChecked = false)
        {
            Item = obj;
            IsChecked = isChecked;
        }

        public T Item
        {
            get { return item; }
            set
            {
                item = value;
                RaisePropertyChanged("Item");
                RaisePropertyChanged("Label");
            }
        }

        public bool IsChecked
        {
            get { return isChecked; }
            set
            {
                isChecked = value;
                RaisePropertyChanged("IsChecked");
            }
        }

        public string Label
        {
            get { return Item.Label; }
        }

        public override bool Equals(object obj)
        {
            CheckBoxItem<T> checkBoxItem = obj as CheckBoxItem<T>;
            if (checkBoxItem == null)
                return false;
            return Item.Equals(checkBoxItem.Item);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}