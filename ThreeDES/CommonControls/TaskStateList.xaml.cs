﻿using System.Collections;
using System.Windows;
using System.Windows.Controls;

namespace CommonControls
{
    /// <summary>
    ///     Interaction logic for TaskStateList.xaml
    /// </summary>
    public partial class TaskStateList : UserControl
    {
        public TaskStateList()
        {
            InitializeComponent();
        }

        #region Dependency Properties

        public static DependencyProperty StateTemplateProperty =
            DependencyProperty.Register("StateTemplate", typeof(DataTemplate), typeof(TaskStateList));

        public DataTemplate StateTemplate
        {
            get { return (DataTemplate) GetValue(StateTemplateProperty); }
            set { SetValue(StateTemplateProperty, value); }
        }

        public static DependencyProperty ItemsProperty =
            DependencyProperty.Register("Items", typeof(IEnumerable), typeof(TaskStateList));

        public IEnumerable Items
        {
            get { return (IEnumerable) GetValue(ItemsProperty); }
            set { SetValue(ItemsProperty, value); }
        }

        #endregion // end of Dependency Properties
    }
}