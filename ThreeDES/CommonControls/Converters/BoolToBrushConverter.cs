﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace CommonControls.Converters
{
    public class BoolToBrushConverter : IValueConverter
    {
        private static string argumentException =
            "Object of unsupported type [{0}] passed to BoolToBrushConverter.{1}()";

        /* true --> YellowGreen, false --> Coral (light red), null --> light yellow */

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return new SolidColorBrush(Color.FromRgb(255, 255, 160));

            if (value is bool)
            {
                bool boolValue = (bool) value;
                return BoolToBrush(boolValue);
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "Convert"), "value");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is Brush)
            {
                Brush brush = (Brush) value;
                return brush.Equals(Brushes.YellowGreen);
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "ConvertBack"), "value");
        }

        private Brush BoolToBrush(bool value)
        {
            return value ? Brushes.YellowGreen : Brushes.Coral;
        }
    }
}