﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CommonControls.Converters
{
    public class NegateBoolConverter : IValueConverter
    {
        private static string argumentException = "Object of unsupported type [{0}] passed to NegateBoolConverter.{1}()";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is bool?)
            {
                bool? nullableValue = (bool?) value;
                if (nullableValue.HasValue)
                    return !nullableValue.Value;
                return null;
            }

            if (value is bool)
                return !(bool) value;

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "Convert/ConvertBack"),
                "value");
        }

        // does exactly the same thing as Convert() ;)
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Convert(value, targetType, parameter, culture);
        }
    }
}