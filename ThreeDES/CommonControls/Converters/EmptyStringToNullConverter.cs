﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CommonControls.Converters
{
    public class EmptyStringToNullConverter : IValueConverter
    {
        private static string argumentException =
            "Object of unsupported type [{0}] passed to EmptyStringToNullConverter.{1}()";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is string)
            {
                string stringValue = (string) value;
                return stringValue != "" ? stringValue : null;
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "Convert"), "value");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null || value is string)
                return value;

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "ConvertBack"), "value");
        }
    }
}