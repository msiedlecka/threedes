﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace CommonControls.Converters
{
    public class FinishedStatusToBoolConverter : IValueConverter
    {
        private static string argumentException =
            "Object of unsupported type [{0}] passed to FinishedStatusToBool.{1}()";

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is FinishedTaskStatus)
            {
                FinishedTaskStatus status = (FinishedTaskStatus) value;
                switch (status)
                {
                    case FinishedTaskStatus.SUCCESS:
                        return true;
                    case FinishedTaskStatus.CANCELLED:
                        return null;
                    case FinishedTaskStatus.FAILURE:
                        return false;
                    default:
                        return false;
                }
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "Convert"), "value");
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
                return null;

            if (value is bool?)
            {
                bool? nullableValue = (bool?) value;
                switch (nullableValue)
                {
                    case true:
                        return FinishedTaskStatus.SUCCESS;
                    case false:
                        return FinishedTaskStatus.FAILURE;
                    case null:
                        return FinishedTaskStatus.CANCELLED;
                    default:
                        return FinishedTaskStatus.FAILURE;
                }
            }

            throw new ArgumentException(string.Format(argumentException, value.GetType().Name, "ConvertBack"), "value");
        }
    }
}