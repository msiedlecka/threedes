﻿using System.Security;
using System.Windows;
using System.Windows.Controls;
using ExtensionMethods;

namespace CommonControls
{
    public partial class SwitchablePasswordBox : UserControl
    {
        public SwitchablePasswordBox()
        {
            InitializeComponent();
        }

        #region Events

        // manually update the textblock value
        void OnPasswordChanged(object sender, RoutedEventArgs args)
        {
            this.GetFirstRecursiveChildOfType<TextBox>().Text = (sender as PasswordBox).Password;
        }

        #endregion // end of Events

        #region Dependency Properties

        #region Password and Text Boxes

        /* 
         * Actually, this shouldn't be done by string, but through SecureString (no plain text in memory) - 
         * there is a readonly property SecurePassword in PasswordBox (not bindable! need a workaround or sth). 
         * But eventually I will need the plain text, so for now I just do it the way I know how to.
         */

        public string PasswordPlainText
        {
            get { return this.GetFirstRecursiveChildOfType<PasswordBox>().Password; }
            set { this.GetFirstRecursiveChildOfType<PasswordBox>().Password = value; }
        }

        public SecureString GetSecurePassword()
        {
            return this.GetFirstRecursiveChildOfType<PasswordBox>().SecurePassword;
        }

        #endregion // end of Password and Text Boxes

        #region Checkbox

        public static DependencyProperty CheckboxTextProperty =
            DependencyProperty.Register("CheckboxText", typeof(string), typeof(SwitchablePasswordBox));

        public string CheckboxText
        {
            get { return (string) GetValue(CheckboxTextProperty); }
            set { SetValue(CheckboxTextProperty, value); }
        }

        #endregion // end of Checkbox

        #endregion // end of Dependency Properties
    }
}