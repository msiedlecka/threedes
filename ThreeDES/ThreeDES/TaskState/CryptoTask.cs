﻿using System;
using System.ComponentModel;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;
using CommonControls;
using ThreeDES.Cryptography;

namespace ThreeDES.TaskState
{
    public abstract class CryptoTask
    {
        protected CancellationTokenSource cancelTokenSource;
        protected CipherEngineBase cipher;

        protected Task crypto;

        protected StringBuilder errorMessages;
        protected byte[] iv;
        protected byte[] key;
        protected Dispatcher mainDispatcher;
        protected CryptoTaskState state;

        public CryptoTask(uint id, CryptoTaskState state)
        {
            Id = id;
            ErrorStatus = FinishedTaskStatus.SUCCESS;
            errorMessages = new StringBuilder();
            this.state = state;
            cancelTokenSource = new CancellationTokenSource();
            ProgressChanged += (obj, args) => { state.ProgressValue = args.ProgressPercentage; };
        }

        protected ProgressChangedEventHandler ProgressChanged { get; set; }

        public uint Id { get; protected set; }

        public FinishedTaskStatus ErrorStatus { get; protected set; }

        public string ErrorMessages
        {
            get { return errorMessages.ToString(); }
        }

        protected abstract string OutputPath { get; }

        public void Start(Dispatcher mainDispatcher)
        {
            this.mainDispatcher = mainDispatcher;
            crypto = Task.Factory.StartNew(() =>
            {
                CipherWork();
                AfterExecuted();
            }, cancelTokenSource.Token);
        }

        public void Cancel()
        {
            ErrorStatus = FinishedTaskStatus.CANCELLED;
            cancelTokenSource.Cancel();
        }

        public void Wait()
        {
            crypto.Wait();
        }

        protected abstract void CipherWork();

        // clean - delete the result file
        protected void CleanOnCancel()
        {
            if (File.Exists(OutputPath))
                File.Delete(OutputPath);
        }

        public void AfterExecuted()
        {
            if (IsCancelled())
                CleanOnCancel();
            if (!CryptoTasksManager.Instance.IsCancelAllRequested())
                mainDispatcher.Invoke((Action) (() => CryptoTasksManager.Instance.OnTaskFinished(Id)), null);
        }

        /* sets 'key' and 'iv' properties */
        protected abstract void PrepareKeyAndIV();

        protected void PrepareCipherEngine(ICipherDataHolder holder, bool encrypt)
        {
            PrepareKeyAndIV();

            if (HasNoErrors())
            {
                cipher = new TripleDesEngine(key, iv);
                cipher.Init(holder, encrypt);
            }
        }

        protected void ReportProgress(long totalBytesRead, long fileSizeInBytes)
        {
            int progress = (int) Math.Floor((double) totalBytesRead*100/fileSizeInBytes);
            ProgressChanged(null, new ProgressChangedEventArgs(progress, null));
        }

        protected void AppendError(string errorMessage)
        {
            ErrorStatus = FinishedTaskStatus.FAILURE;
            errorMessages.AppendLine(errorMessage);
        }

        protected bool HasNoErrors()
        {
            return !ErrorStatus.Equals(FinishedTaskStatus.FAILURE);
        }

        protected bool IsCancelled()
        {
            return cancelTokenSource.IsCancellationRequested;
        }
    }
}