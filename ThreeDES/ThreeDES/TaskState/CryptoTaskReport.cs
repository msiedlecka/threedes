﻿using CommonControls;
using ThreeDES.Commands;
using ThreeDES.Languages;

namespace ThreeDES.TaskState
{
    public class CryptoTaskReport : PropertyChangedNotifier, ILabeled
    {
        private FinishedTaskStatus errorStatus;
        private string label;

        private ShowErrorsCommand seeErrorCommand;
        private string toolTip;

        public CryptoTaskReport(string operationLabel, string operationToolTip, FinishedTaskStatus errorStatus,
            string errorMessages)
        {
            Label = CreateLabel(operationLabel, errorStatus);
            ToolTip = operationToolTip;
            ErrorStatus = errorStatus;
            SeeErrorCommand = new ShowErrorsCommand(errorMessages);
        }

        private string CreateLabel(string operationLabel, FinishedTaskStatus state)
        {
            string status = (state.Equals(FinishedTaskStatus.CANCELLED)
                ? Lang.Cancelled
                : (state.Equals(FinishedTaskStatus.FAILURE) ? Lang.Failure : Lang.Success));
            return string.Format("{0}: {1}", operationLabel, status);
        }

        #region Properties

        public FinishedTaskStatus ErrorStatus
        {
            get { return errorStatus; }
            private set
            {
                errorStatus = value;
                RaisePropertyChanged("ErrorStatus");
            }
        }

        public ShowErrorsCommand SeeErrorCommand
        {
            get { return seeErrorCommand; }
            private set
            {
                seeErrorCommand = value;
                RaisePropertyChanged("SeeErrorCommand");
            }
        }

        public string Label
        {
            get { return label; }
            private set
            {
                label = value;
                RaisePropertyChanged("Label");
            }
        }

        public string ToolTip
        {
            get { return toolTip; }
            private set
            {
                toolTip = value;
                RaisePropertyChanged("ToolTip");
            }
        }

        #endregion  // end of Properties
    }
}