﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using ThreeDES.Cryptography;
using ThreeDES.Cryptography.Decryption;
using ThreeDES.Cryptography.Encryption;

namespace ThreeDES.TaskState
{
    public class CryptoTasksManager : DependencyObject
    {
        public static readonly DependencyProperty TaskStateListProperty =
            DependencyProperty.Register("TaskStateList", typeof(ObservableCollection<CryptoTaskState>),
                typeof(CryptoTasksManager));

        public static readonly DependencyProperty TaskReportListProperty =
            DependencyProperty.Register("TaskReportList", typeof(ObservableCollection<CryptoTaskReport>),
                typeof(CryptoTasksManager));

        private bool cancelAllRequested;
        private object cancelAllSynchronizer = new object();
        private uint id;
        private object idSynchronizer;
        private object taskReportListSynchronizer;
        private object tasksListSynchronizer;

        private object taskStateListSynchronizer;

        static CryptoTasksManager()
        {
            Instance = new CryptoTasksManager();
        }

        private CryptoTasksManager()
        {
            Tasks = new List<CryptoTask>();
            TaskStateList = new ObservableCollection<CryptoTaskState>();
            TaskReportList = new ObservableCollection<CryptoTaskReport>();
            id = 0;
            idSynchronizer = new object();
            taskStateListSynchronizer = new object();
            tasksListSynchronizer = new object();
            taskReportListSynchronizer = new object();
        }

        public List<CryptoTask> Tasks { get; set; }

        public ObservableCollection<CryptoTaskState> TaskStateList
        {
            get { return (ObservableCollection<CryptoTaskState>) GetValue(TaskStateListProperty); }
            set { SetValue(TaskStateListProperty, value); }
        }

        public ObservableCollection<CryptoTaskReport> TaskReportList
        {
            get { return (ObservableCollection<CryptoTaskReport>) GetValue(TaskReportListProperty); }
            set { SetValue(TaskReportListProperty, value); }
        }

        public static CryptoTasksManager Instance { get; private set; }

        private uint GenerateId()
        {
            lock (idSynchronizer)
            {
                return id++;
            }
        }

        private CryptoTaskState CreateAndAddState(IInputOutputInfo data, bool isDecryption = false)
        {
            bool stateAdded = false;
            uint id = GenerateId();
            CryptoTaskState state = new CryptoTaskState(data, id, isDecryption);

            lock (cancelAllSynchronizer)
            {
                if (!cancelAllRequested)
                    lock (taskStateListSynchronizer)
                    {
                        TaskStateList.Add(state);
                        stateAdded = true;
                    }
            }
            return stateAdded ? state : null;
        }

        private CryptoTask CreateTask(EncryptionData data)
        {
            CryptoTaskState state = CreateAndAddState(data, false);
            return state != null ? new EncryptionTask(state.Id, state, data) : null;
        }

        private CryptoTask CreateTask(DecryptionData data)
        {
            CryptoTaskState state = CreateAndAddState(data, true);
            return new DecryptionTask(state.Id, state, data);
        }

        private void StartTask(CryptoTask task)
        {
            if (task != null)
                lock (cancelAllSynchronizer)
                {
                    if (!cancelAllRequested)
                        lock (tasksListSynchronizer)
                        {
                            Tasks.Add(task);
                            task.Start(Dispatcher);
                        }
                }
        }

        public void AddTask(EncryptionData data)
        {
            StartTask(CreateTask(data));
        }

        public void AddTask(DecryptionData data)
        {
            StartTask(CreateTask(data));
        }

        public void OnTaskFinished(uint id)
        {
            CryptoTaskState state;
            CryptoTask task;

            lock (taskStateListSynchronizer)
            {
                state = GetStateById(id);
                TaskStateList.Remove(state);
            }
            lock (tasksListSynchronizer)
            {
                task = GetTaskById(id);
                Tasks.Remove(task);
            }
            lock (taskReportListSynchronizer)
            {
                TaskReportList.Add(new CryptoTaskReport(state.Label, state.ToolTip, task.ErrorStatus, task.ErrorMessages));
            }
        }

        public void CancelTask(uint id)
        {
            CryptoTask task;
            lock (tasksListSynchronizer)
            {
                task = GetTaskById(id);
            }

            if (task != null)
                task.Cancel();
        }

        public void CancelAllTasks()
        {
            List<CryptoTask> cryptoTaskTable;
            lock (cancelAllSynchronizer)
            {
                cancelAllRequested = true;
                lock (tasksListSynchronizer)
                {
                    cryptoTaskTable = Tasks.ToList();
                    foreach (CryptoTask task in Tasks)
                        task.Cancel();
                }
            }

            foreach (CryptoTask task in cryptoTaskTable)
                task.Wait();
        }

        public bool AreAnyTasksInProgress()
        {
            lock (tasksListSynchronizer)
            {
                return Tasks.Count > 0;
            }
        }

        public bool IsCancelAllRequested()
        {
            lock (cancelAllSynchronizer)
            {
                return cancelAllRequested;
            }
        }

        private CryptoTask GetTaskById(uint id)
        {
            return Tasks.Find(task => task.Id == id);
        }

        private CryptoTaskState GetStateById(uint id)
        {
            return TaskStateList.First(state => state.Id == id);
        }
    }
}