﻿using System;
using System.IO;
using Org.BouncyCastle.Crypto;
using ThreeDES.Cryptography.Decryption;
using ThreeDES.FileHandlers;
using ThreeDES.Helpers;

namespace ThreeDES.TaskState
{
    public class DecryptionTask : CryptoTask
    {
        private DecryptionData operationData;

        public DecryptionTask(uint id, CryptoTaskState state, DecryptionData data) : base(id, state)
        {
            operationData = data;
        }

        protected override string OutputPath
        {
            get { return operationData.OutputPath; }
        }

        protected override void CipherWork()
        {
            PrepareCipherEngine(operationData, false);
            if (HasNoErrors())
                Decrypt();
        }

        protected override void PrepareKeyAndIV()
        {
            key = operationData.Recipient.DecryptSessionKey(operationData.PasswordToPrivateKey);
            iv = operationData.InitializationVector;

            operationData.Recipient.ForgetPrivateKey();
            if (key == null)
                AppendError("Error occured while retrieving session key.");
        }

        private void Decrypt()
        {
            ICloseableEncryptedFileParser parser;
            int chunkSize = SettingsHelper.GetChunkSizeInBytes();
            int numberOfDecryptedBytes = 0;
            byte[] encryptedChunk;
            byte[] outputBuffer = new byte[cipher.GetOutputSize(chunkSize)];
            byte[] final;
            long totalBytesProcessed = 0;
            long targetFileLength = 0;

            parser = FileParserFactory.CreateParser(operationData.InputPath);
            if (parser != null)
                try
                {
                    long encryptedContentLength = parser.GetContentLength();
                    using (
                        FileStream outfs = new FileStream(operationData.OutputPath, FileMode.Append, FileAccess.Write))
                    {
                        using (BinaryWriter bw = new BinaryWriter(outfs, SettingsHelper.GetDefaultFileEncoding()))
                        {
                            if (parser.Open())
                            {
                                targetFileLength = parser.GetContentLength();
                                try
                                {
                                    while ((encryptedChunk = parser.ReadNextBytes(chunkSize)) != null && !IsCancelled())
                                    {
                                        numberOfDecryptedBytes = cipher.ProcessBytes(encryptedChunk, 0,
                                            encryptedChunk.Length, outputBuffer, 0);
                                        bw.Write(outputBuffer, 0, numberOfDecryptedBytes);
                                        totalBytesProcessed += encryptedChunk.Length;
                                        ReportProgress(totalBytesProcessed, targetFileLength);
                                    }
                                    if (!IsCancelled())
                                    {
                                        final = cipher.DoFinal();
                                        bw.Write(final, 0, final.Length);
                                    }
                                }
                                catch (DataLengthException dle)
                                {
                                    /* MessageBox.Show(String.Format("The encrypted content in file {0} is of incorrect length. {1}", Path.GetFileName(operationData.InputPath),
                                     CertificateHelper.FileIsCorrupted), Languages.Lang.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                                 AppendError("The encrypted content is of incorrect length.");*/
                                }
                                catch (InvalidCipherTextException icte)
                                {
                                    /*MessageBox.Show(String.Format("The encrypted content in file {0} is of incorrect length. {1}", Path.GetFileName(operationData.InputPath),
                                    CertificateHelper.FileIsCorrupted), Languages.Lang.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                                AppendError("The encrypted content is of incorrect length.");*/
                                }
                                catch (Exception ex)
                                {
                                    /*MessageBox.Show(String.Format("Error occured during decryption of {0}. {1}", Path.GetFileName(operationData.InputPath),
                                    CertificateHelper.FileIsCorrupted), Languages.Lang.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                                AppendError(String.Format("Error occured during decryption: {0}", ex.Message));*/
                                }
                                finally
                                {
                                    parser.Close();
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    AppendError(string.Format("Error occured during accessing input ({0}) or output ({1}) file: {2}",
                        Path.GetFileName(operationData.InputPath), Path.GetFileName(OutputPath), ex.Message));
                }
        }
    }
}