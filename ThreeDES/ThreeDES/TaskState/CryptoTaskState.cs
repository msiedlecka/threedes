﻿using System.IO;
using CommonControls;
using ThreeDES.Commands;
using ThreeDES.Cryptography;
using ThreeDES.Languages;

namespace ThreeDES.TaskState
{
    public class CryptoTaskState : PropertyChangedNotifier, ILabeled
    {
        private string operationName;
        private string processedFileName;
        private string processedFilePath;
        private double progressValue;

        public CryptoTaskState(IInputOutputInfo taskBasicInfo, uint id, bool isDecryption = false)
        {
            operationName = isDecryption ? Lang.Decryption : Lang.Encryption;
            processedFilePath = taskBasicInfo.InputPath;
            processedFileName = Path.GetFileName(taskBasicInfo.InputPath);
            ProgressValue = 0;
            Id = id;
            CancelCommand = new CancelCryptoTaskCommand(id);
        }

        public string ToolTip
        {
            get { return processedFilePath; }
        }

        public double ProgressValue
        {
            get { return progressValue; }
            set
            {
                progressValue = value;
                RaisePropertyChanged("ProgressValue");
            }
        }

        public uint Id { get; set; }

        public CancelCryptoTaskCommand CancelCommand { get; set; }

        public string Label
        {
            get { return string.Format("{0} ({1})", operationName, processedFileName); }
        }
    }
}