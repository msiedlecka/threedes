﻿using System;
using System.IO;
using ThreeDES.Cryptography;
using ThreeDES.Cryptography.Data;
using ThreeDES.Cryptography.Encryption;
using ThreeDES.FileHandlers;
using ThreeDES.Helpers;

namespace ThreeDES.TaskState
{
    public class EncryptionTask : CryptoTask
    {
        private EncryptionData operationData;

        public EncryptionTask(uint id, CryptoTaskState state, EncryptionData data) : base(id, state)
        {
            operationData = data;
        }

        protected override string OutputPath
        {
            get { return operationData.OutputPath; }
        }

        protected override void CipherWork()
        {
            PrepareCipherEngine(operationData, true);
            GiveKeysToRecipients();
            try
            {
                SaveHeader();
            }
            catch (Exception ex)
            {
                AppendError(string.Format("Error occured during saving the header in the file {0}: {1}",
                    Path.GetFileName(OutputPath), ex.Message));
            }

            if (HasNoErrors())
                Encrypt();
        }

        protected override void PrepareKeyAndIV()
        {
            CipherEngineBase generator = new TripleDesEngine();
            key = generator.GenerateKey();
            iv = generator.GenerateIV();
        }

        private void Encrypt()
        {
            int chunkSize = SettingsHelper.GetChunkSizeInBytes();
            int bytesRead = chunkSize;
            int numberOfEncryptedBytes = 0;
            byte[] chunk = new byte[chunkSize];
            byte[] outputBuffer = new byte[cipher.GetOutputSize(chunkSize)];
            byte[] final;
            long totalBytesProcessed = 0;
            long fileLength = 0;

            try
            {
                using (FileStream infs = new FileStream(operationData.InputPath, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(infs))
                    {
                        using (
                            FileStream outfs = new FileStream(operationData.OutputPath, FileMode.Append,
                                FileAccess.Write))
                        {
                            using (BinaryWriter bw = new BinaryWriter(outfs, SettingsHelper.GetDefaultFileEncoding()))
                            {
                                fileLength = infs.Length;
                                try
                                {
                                    while (bytesRead > 0 && !IsCancelled())
                                    {
                                        bytesRead = br.Read(chunk, 0, chunkSize);
                                        numberOfEncryptedBytes = cipher.ProcessBytes(chunk, 0, bytesRead, outputBuffer,
                                            0);
                                        bw.Write(outputBuffer, 0, numberOfEncryptedBytes);
                                        totalBytesProcessed += bytesRead;
                                        ReportProgress(totalBytesProcessed, fileLength);
                                    }
                                    if (!IsCancelled())
                                    {
                                        final = cipher.DoFinal();
                                        bw.Write(final, 0, final.Length);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    AppendError(string.Format("Error occured during encryption: {0}", ex.Message));
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                AppendError(string.Format("Error occured during accessing the file {0}: {1}",
                    Path.GetFileName(operationData.InputPath), ex.Message));
            }
        }

        // gives generated session key to all recipients and they encrypt it with their public keys
        private void GiveKeysToRecipients()
        {
            foreach (Recipient recipient in operationData.Recipients)
                recipient.EncryptPlainSessionKey(key);
        }

        private void SaveHeader()
        {
            string extension = Path.GetExtension(operationData.OutputPath);
            EncryptedFileHeader header = new EncryptedFileHeader(operationData, iv);
            IEncryptedFileSaver saver = FileSaverFactory.CreateSaver(extension);
            if (saver != null)
                saver.SaveHeader(header, operationData.OutputPath);
            else
                AppendError(ExtensionsHelper.CreateExtensionErrorMessage(extension));
        }
    }
}