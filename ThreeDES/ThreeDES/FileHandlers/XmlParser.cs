﻿using System;
using System.IO;
using System.Text;
using System.Windows;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using ThreeDES.Cryptography.Data;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.FileHandlers
{
    public class XmlParser : ICloseableEncryptedFileParser
    {
        public XmlParser(string sourcePath)
        {
            SourcePath = sourcePath;
        }

        public string SourcePath { get; private set; }

        public EncryptedFileHeader ParseHeader()
        {
            EncryptedFileHeader header;
            Validate();
            if (validationSuccess)
            {
                XmlSerializer deserializer = new XmlSerializer(typeof(EncryptedFileHeader));

                using (XmlReader reader = XmlReader.Create(SourcePath, settings))
                {
                    reader.ReadToFollowing(SettingsHelper.GetXmlHeaderName());
                    using (XmlReader inner = reader.ReadSubtree())
                    {
                        header = (EncryptedFileHeader) deserializer.Deserialize(inner);
                    }
                }

                // logical consistency check
                if (header.IsValid())
                    return header;

                return null;
            }
            InformAboutValidationErrors(Lang.ValidationError);
            return null;
        }

        /*
         * Returns chunk of encrypted data, containing 'length' bytes. 
         * StartPosition is a zero-based index, counted from the beginning 
         * of the content section, NOT the whole file.
         */

        public byte[] ReadPieceOfContent(long startPosition, int length)
        {
            long positionAfterMoving = 0;
            int bytesToRead;

            if (contentStartPosition == ContentStartPositionNotDetected)
                FindContentStartPosition();

            using (FileStream fs = new FileStream(SourcePath, FileMode.OpenOrCreate, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs, SettingsHelper.GetDefaultFileEncoding()))
                {
                    positionAfterMoving = br.BaseStream.Seek(contentStartPosition + startPosition, SeekOrigin.Begin);
                    if (positionAfterMoving == contentStartPosition + startPosition)
                    {
                        bytesToRead = (int) Math.Min(length, br.BaseStream.Length - positionAfterMoving);
                        return ReadNextFromBinary(br, bytesToRead);
                    }
                    return null; // seeking made a skip shorter than 'startPosition' - means that EOF was just reached
                }
            }
        }

        public long GetContentLength()
        {
            if (contentStartPosition == ContentStartPositionNotDetected)
                FindContentStartPosition();

            FileInfo fi = new FileInfo(SourcePath);
            return fi.Length - contentStartPosition;
        }

        /* Open() must be called before calling this method */

        public byte[] ReadNextBytes(int chunkLength)
        {
            // (contentStartPosition + currentPositionInContent) = position in the WHOLE file
            int bytesToRead =
                (int)
                Math.Min(chunkLength,
                    sourceBinaryReader.BaseStream.Length - (contentStartPosition + currentPositionInContent));
            currentPositionInContent += bytesToRead;
            return ReadNextFromBinary(sourceBinaryReader, bytesToRead);
        }

        public bool Open()
        {
            try
            {
                FindContentStartPosition();
                sourceFileStream = new FileStream(SourcePath, FileMode.OpenOrCreate, FileAccess.Read);
                sourceBinaryReader = new BinaryReader(sourceFileStream, SettingsHelper.GetDefaultFileEncoding());
                sourceBinaryReader.BaseStream.Seek(contentStartPosition, SeekOrigin.Begin);
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Error occured while opening file {0}. See message below. {1}{2}",
                        Path.GetFileName(SourcePath), Environment.NewLine, ex.Message), Lang.Error, MessageBoxButton.OK,
                    MessageBoxImage.Error);
                return false;
            }
        }

        /* must be called when job with the file (SourcePath) is done, if Open() was called and completed successfully (returned true) */

        public void Close()
        {
            if (sourceBinaryReader != null)
            {
                sourceBinaryReader.Close();
                sourceBinaryReader.Dispose();
            }
        }

        /* provided BinaryReader must be prevoiusly open; it's not closed */

        private static byte[] ReadNextFromBinary(BinaryReader br, int length)
        {
            if (length > 0)
            {
                byte[] buffer = new byte[length];
                br.Read(buffer, 0, length);
                return buffer;
            }
            return null;
        }

        #region Moving To Content

        private void FindContentStartPosition()
        {
            Encoding enc = SettingsHelper.GetDefaultFileEncoding();
            long currentPosition = 0;
            int chunkSize = 50;
            int bufferStartPosition = 0;
            int headerEndTagStartPosition = 0;
            byte[] fileChunk = new byte[chunkSize*2];
                // two last chunks are kept so that xml end element will not be missed (if divided in two chunks)
            string currentStringResult;
            string headerEndTag = XmlHelper.CreateEndElement(SettingsHelper.GetXmlHeaderName()) + Environment.NewLine;

            using (FileStream fs = new FileStream(SourcePath, FileMode.OpenOrCreate, FileAccess.Read))
            {
                using (BinaryReader br = new BinaryReader(fs, enc))
                {
                    // while there is something to read and start position of the encrypted binary content has not been detected yet
                    while (br.Read(fileChunk, bufferStartPosition, chunkSize) != -1 &&
                           contentStartPosition == ContentStartPositionNotDetected)
                        if (bufferStartPosition == chunkSize)
                        {
                            currentStringResult = enc.GetString(fileChunk);
                            headerEndTagStartPosition = currentStringResult.LastIndexOf(headerEndTag,
                                StringComparison.InvariantCulture);
                            if (headerEndTagStartPosition != -1)
                                contentStartPosition = currentPosition + headerEndTagStartPosition +
                                                       enc.GetByteCount(headerEndTag);
                            else
                                Buffer.BlockCopy(fileChunk, bufferStartPosition, fileChunk, 0, chunkSize);
                            currentPosition += chunkSize;
                        }
                        else bufferStartPosition = chunkSize;
                }
            }
        }

        #endregion // end of Moving To Content

        #region Fields

        private const long ContentStartPositionNotDetected = -1;

        private bool validationSuccess = true;
        private StringBuilder errors = new StringBuilder();
        private XmlReaderSettings settings;

        private long contentStartPosition = ContentStartPositionNotDetected; // counted from the beginning of the file
        private long currentPositionInContent;
        private FileStream sourceFileStream;
        private BinaryReader sourceBinaryReader;

        #endregion // end of Fields

        #region Validation Implementation

        private void ValidationCallback(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                errors.AppendFormat("Warning: Matching schema not found. No validation occurred. Message: {0}",
                    args.Message + Environment.NewLine);
            else
            {
                validationSuccess = false;
                AppendErrorInformation(args.Exception.LineNumber, args.Exception.LinePosition, args.Message);
            }
        }

        private void Validate()
        {
            SetValidationSettings();
            ValidateXml(settings);
        }

        private void SetValidationSettings()
        {
            settings = new XmlReaderSettings();
            settings.Schemas.Add(null, "Resources/TripleDESSchema.xsd");
            settings.ValidationType = ValidationType.Schema;
            settings.ValidationEventHandler += ValidationCallback;
        }

        private void ValidateXml(XmlReaderSettings settings)
        {
            try
            {
                using (XmlReader reader = XmlReader.Create(SourcePath, settings))
                {
                    while (reader.Read() &&
                           !(reader.LocalName.Equals(SettingsHelper.GetXmlHeaderName()) &&
                             reader.NodeType.Equals(XmlNodeType.EndElement))) ;
                }
            }
            catch (XmlException xe)
            {
                validationSuccess = false;
                AppendErrorInformation(xe.LineNumber, xe.LinePosition, xe.Message);
            }
        }

        private void AppendErrorInformation(int lineNumber, int position, string message)
        {
            errors.AppendFormat("Validation error (line {0}, position {1}): {2}", lineNumber,
                position, message + Environment.NewLine);
        }

        private void InformAboutValidationErrors(string header)
        {
            MessageBox.Show(Lang.GivenFileIsNotCorrect + Environment.NewLine + errors, header,
                MessageBoxButton.OK, MessageBoxImage.Error);
        }

        #endregion // end of Validation Implementation
    }
}