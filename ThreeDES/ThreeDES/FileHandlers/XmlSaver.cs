﻿using System;
using System.IO;
using System.Text;
using System.Xml.Serialization;
using ThreeDES.Cryptography.Data;
using ThreeDES.Helpers;

namespace ThreeDES.FileHandlers
{
    public class XmlSaver : IEncryptedFileSaver
    {
        public void SaveHeader(EncryptedFileHeader header, string destinationPath)
        {
            using (FileStream fs = File.Create(destinationPath))
            {
                XmlSerializer serializer = new XmlSerializer(typeof(EncryptedFileHeader));
                serializer.Serialize(fs, header);
                AppendNewLineCharacter(fs);
            }
        }

        public void AppendPieceOfContent(byte[] pieceOfContent, string destinationPath)
        {
            using (FileStream fw = new FileStream(destinationPath, FileMode.Append, FileAccess.Write))
            {
                using (BinaryWriter bw = new BinaryWriter(fw, SettingsHelper.GetDefaultFileEncoding()))
                {
                    bw.Write(pieceOfContent);
                }
            }
        }

        private void AppendNewLineCharacter(FileStream fs)
        {
            byte[] newLine = Encoding.UTF8.GetBytes(Environment.NewLine);
            int lengthOfNewLine = newLine.Length;
            if (fs.CanWrite)
                fs.Write(newLine, 0, lengthOfNewLine);
        }
    }
}