﻿using System.Windows;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.FileHandlers
{
    public abstract class FileSaverFactory
    {
        public static IEncryptedFileSaver CreateSaver(string fileExtension)
        {
            if (ExtensionsHelper.IsExtensionSupported(fileExtension, SettingsHelper.GetFormatFilterForEncrypted()))
            {
                switch (fileExtension)
                {
                    case ".xml":
                    case "":
                        return new XmlSaver();
                    default:
                        MessageBox.Show(ExtensionsHelper.NotImplementedExceptionDescription, Lang.Information,
                            MessageBoxButton.OK, MessageBoxImage.Information);
                        return null;
                }
            }
            MessageBox.Show(ExtensionsHelper.ArgumentExceptionDescription, Lang.Error,
                MessageBoxButton.OK, MessageBoxImage.Error);
            return null;
        }
    }
}