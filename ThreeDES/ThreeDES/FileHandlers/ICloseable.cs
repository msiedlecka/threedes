﻿namespace ThreeDES.FileHandlers
{
    public interface ICloseable
    {
        bool Open(); // returning true means everything is OK
        void Close();
    }
}