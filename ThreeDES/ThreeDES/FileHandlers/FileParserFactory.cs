﻿using System.IO;
using System.Windows;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.FileHandlers
{
    public abstract class FileParserFactory
    {
        public static ICloseableEncryptedFileParser CreateParser(string filePath)
        {
            string fileExtension = Path.GetExtension(filePath);
            if (ExtensionsHelper.IsExtensionSupported(fileExtension, SettingsHelper.GetFormatFilterForEncrypted()))
            {
                switch (fileExtension)
                {
                    case ".xml":
                    case "":
                        return new XmlParser(filePath);
                    default:
                        MessageBox.Show(ExtensionsHelper.NotImplementedExceptionDescription, Lang.Information,
                            MessageBoxButton.OK, MessageBoxImage.Information);
                        return null;
                }
            }
            MessageBox.Show(ExtensionsHelper.ArgumentExceptionDescription, Lang.Error,
                MessageBoxButton.OK, MessageBoxImage.Error);
            return null;
        }
    }
}