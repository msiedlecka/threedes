﻿using ThreeDES.Cryptography.Data;

namespace ThreeDES.FileHandlers
{
    public interface IEncryptedFileSaver
    {
        void SaveHeader(EncryptedFileHeader header, string destinationPath);

        // doesn't seem useful, certainly NOT efficient in loops (open/close file just to append some bytes, nigthmare)
        void AppendPieceOfContent(byte[] pieceOfContent, string destinationPath);
    }
}