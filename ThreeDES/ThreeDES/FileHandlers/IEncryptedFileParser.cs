﻿using ThreeDES.Cryptography.Data;

namespace ThreeDES.FileHandlers
{
    public interface IEncryptedFileParser
    {
        string SourcePath { get; }

        EncryptedFileHeader ParseHeader();

        byte[] ReadPieceOfContent(long startPosition, int length);

        long GetContentLength();
    }
}