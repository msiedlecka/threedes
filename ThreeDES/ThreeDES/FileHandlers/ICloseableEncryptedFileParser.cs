﻿namespace ThreeDES.FileHandlers
{
    public interface ICloseableEncryptedFileParser : IEncryptedFileParser, ICloseable
    {
        byte[] ReadNextBytes(int chunkLength);
    }
}