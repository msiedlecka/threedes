﻿using System;
using System.Windows;
using System.Windows.Input;
using CommonControls;
using ThreeDES.Cryptography.Decryption;
using ThreeDES.Languages;
using ThreeDES.TaskState;

namespace ThreeDES.Commands
{
    public class StartDecryptionCommand : HotKeyCommand, ICommand
    {
        public Func<DecryptionData> BeforeExecute;

        public StartDecryptionCommand()
        {
            Text = Lang.StartDecryption;
            Gesture = new KeyGesture(Key.D, ModifierKeys.Control);
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            SwitchablePasswordBox passwordHolder = parameter as SwitchablePasswordBox;
            string password;

            if (passwordHolder != null)
            {
                password = passwordHolder.PasswordPlainText;
                if (password != null && password != "")
                {
                    if (BeforeExecute != null)
                    {
                        DecryptionData data = BeforeExecute.Invoke();
                        // if data is null, then there must be something missing; 
                        // DecryptionViewModel knows what's wrong and reports it to the user
                        if (data != null)
                        {
                            data.PasswordToPrivateKey = password;
                            CryptoTasksManager.Instance.AddTask(data);
                        }
                    }
                }
                else
                {
                    MessageBox.Show(
                        string.Format(
                            "There is not enough information provided for decryption to start. See messages below.{0}{1}",
                            Environment.NewLine, "The password to private key is missing. Please provide a valid one. "),
                        Lang.NotEnoughInfo, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                }
            }
        }
    }
}