﻿using System;
using System.Windows;
using System.Windows.Input;
using ThreeDES.Languages;

namespace ThreeDES.Commands
{
    public class ShowAboutCommand : HotKeyCommand, ICommand
    {
        public ShowAboutCommand()
        {
            Text = Lang.About;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            MessageBox.Show(Lang.ProjectSCS1415 + Environment.NewLine + Lang.AuthorMe,
                Lang.About, MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}