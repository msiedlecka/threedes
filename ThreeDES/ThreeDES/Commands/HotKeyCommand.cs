﻿using System.Globalization;
using System.Windows.Input;

namespace ThreeDES.Commands
{
    public abstract class HotKeyCommand
    {
        public KeyGesture Gesture { get; set; }

        public string GestureText
        {
            get { return Gesture != null ? Gesture.GetDisplayStringForCulture(CultureInfo.CurrentUICulture) : ""; }
        }

        public string Text { get; set; }

        public string ToolTip
        {
            get { return GestureText.Equals("") ? Text : Text + string.Format(" ({0})", GestureText); }
        }
    }
}