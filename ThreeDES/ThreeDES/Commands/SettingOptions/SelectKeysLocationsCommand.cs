﻿using System;
using System.Windows;
using System.Windows.Input;
using ThreeDES.Languages;
using ThreeDES.ViewModels.CertificatePathsChooser;
using ThreeDES.Views;

namespace ThreeDES.Commands.SettingOptions
{
    public class SelectKeysLocationsCommand : HotKeyCommand, ICommand
    {
        public Action AfterExecuted;

        public SelectKeysLocationsCommand()
        {
            Text = Lang.ChooseKeysLocalizations;
            Gesture = new KeyGesture(Key.K, ModifierKeys.Control);
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            KeyLocationsViewModel locationsViewModel = new KeyLocationsViewModel();
            DefaultLocationsPickerView locationsView = new DefaultLocationsPickerView
            {
                DataContext = locationsViewModel,
                Owner = parameter as Window
            };
            locationsView.Closing += locationsViewModel.OnViewClosing;
            locationsView.ShowDialog();
            if (AfterExecuted != null)
                AfterExecuted.Invoke();
        }
    }
}