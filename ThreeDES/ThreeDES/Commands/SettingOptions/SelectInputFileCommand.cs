﻿using System;
using System.Collections.Specialized;
using System.Windows.Input;
using Microsoft.Win32;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.Commands.SettingOptions
{
    public class SelectInputFileCommand : HotKeyCommand, ICommand
    {
        private StringCollection formatFilter;

        public SelectInputFileCommand(StringCollection filter, bool mergeWithJokerExtension = true)
        {
            Text = Lang.SelectInputFile;
            formatFilter = mergeWithJokerExtension
                ? SettingsHelper.Merge(filter, SettingsHelper.GetJokerFormatFilter())
                : filter;
        }

        public SelectInputFileCommand() : this(SettingsHelper.GetJokerFormatFilter(), false)
        {
        }

        public Action<string> AfterExecuted { get; set; }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.CheckFileExists = true;
            openFileDialog.CheckPathExists = true;
            openFileDialog.Multiselect = false;
            openFileDialog.DereferenceLinks = true;
            openFileDialog.Title = Lang.SelectInputFile;
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
            openFileDialog.Filter = FileDialogHelper.CreateFormatsFilter(formatFilter);

            bool? userDidChooseFile = openFileDialog.ShowDialog();
            if (userDidChooseFile == true)
                if (AfterExecuted != null)
                    AfterExecuted.Invoke(openFileDialog.FileName);
        }
    }
}