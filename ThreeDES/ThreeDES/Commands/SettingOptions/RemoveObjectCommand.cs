﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Input;

namespace ThreeDES.Commands.SettingOptions
{
    public class RemoveObjectCommand<T> : HotKeyCommand, ICommand
    {
        public RemoveObjectCommand(string commandTitle, ICollection<T> collection)
        {
            Text = commandTitle;
            Collection = collection;
            SelectedIndex = Collection.Count > 0 ? 0 : -1;
        }

        public int SelectedIndex { get; set; }

        public ICollection<T> Collection { get; set; }

        public bool CanExecute(object parameter)
        {
            return (SelectedIndex >= 0 && SelectedIndex < Collection.Count);
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        // correct conditions should be provided by CanExecute()
        public void Execute(object parameter)
        {
            T item = Collection.ElementAt(SelectedIndex);
            Collection.Remove(item);
        }
    }
}