﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.Commands.SettingOptions
{
    public class ImportFromFolderCommand : HotKeyCommand, ICommand
    {
        private bool withPrivateKeys;

        public ImportFromFolderCommand(bool withPrivateKeys = false)
        {
            Text = Lang.ChooseFolderToImport;
            this.withPrivateKeys = withPrivateKeys;
        }

        public Action<List<string>> AfterExecuted { get; set; }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            string folderPath;
            List<string> folderPathSingle = new List<string>();
            List<string> certPaths;

            folderPath = FileDialogHelper.ChooseFolder();
            if (folderPath != null)
            {
                folderPathSingle.Add(folderPath);
                certPaths = CertificateHelper.GetCertificatesPathsFromLocations(folderPathSingle, withPrivateKeys);
                if (AfterExecuted != null)
                    AfterExecuted.Invoke(certPaths);
            }
        }
    }
}