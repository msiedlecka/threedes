﻿using System;
using System.Windows.Input;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.Commands.SettingOptions
{
    public class ChooseFolderCommand : HotKeyCommand, ICommand
    {
        public Action<string> AfterExecuted;

        public ChooseFolderCommand()
        {
            Text = Lang.ChooseFolder;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            string folderPath = FileDialogHelper.ChooseFolder();
            if (AfterExecuted != null)
                AfterExecuted.Invoke(folderPath);
        }
    }
}