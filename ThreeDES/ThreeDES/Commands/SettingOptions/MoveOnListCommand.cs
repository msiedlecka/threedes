﻿using System;
using System.Collections;
using System.Windows.Input;
using ExtensionMethods;

namespace ThreeDES.Commands.SettingOptions
{
    public class MoveOnListCommand : HotKeyCommand, ICommand
    {
        public Action AfterExecuted;

        public MoveOnListCommand(string commandTitle, IList list, bool moveForward)
        {
            Text = commandTitle;
            List = list;
            MoveForward = moveForward;
            SelectedIndex = -1;
        }

        public int SelectedIndex { get; set; }

        public IList List { get; set; }

        private bool MoveForward { get; set; }

        public bool CanExecute(object parameter)
        {
            if (SelectedIndex >= 0)
            {
                int collectionCount = List.Count;
                if ((MoveForward && SelectedIndex < collectionCount - 1) || (!MoveForward && SelectedIndex > 0))
                    return true;
            }
            return false;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            int indexToSwapWith = MoveForward ? SelectedIndex++ : SelectedIndex--;
            List.Swap(SelectedIndex, indexToSwapWith);
            if (AfterExecuted != null)
                AfterExecuted.Invoke();
        }
    }
}