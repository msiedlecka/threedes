﻿using System;
using System.Collections.Generic;
using System.Windows.Input;
using CommonControls;
using ThreeDES.Cryptography.Data;

namespace ThreeDES.Commands.SettingOptions
{
    public class MoveCheckedUp : HotKeyCommand, ICommand
    {
        public MoveCheckedUp(string commandTitle, IList<CheckBoxItem<Recipient>> list)
        {
            Text = commandTitle;
            List = list;
        }

        public IList<CheckBoxItem<Recipient>> List { get; set; }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            IList<CheckBoxItem<Recipient>> checkedItems = new List<CheckBoxItem<Recipient>>();
            int listLength = List.Count;

            // finds checked items and removes them from the list
            for (int i = 0; i < listLength; i++)
                if (List[i].IsChecked)
                {
                    checkedItems.Add(List[i]);
                    List.RemoveAt(i);
                    listLength--;
                    i--;
                }

            int numberOfCheckedItems = checkedItems.Count;

            // inserts checked items at the beginning of the list
            for (int i = numberOfCheckedItems - 1; i >= 0; i--)
                List.Insert(0, checkedItems[i]);
        }
    }
}