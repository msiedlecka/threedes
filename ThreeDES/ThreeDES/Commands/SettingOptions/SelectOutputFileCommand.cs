﻿using System;
using System.Collections.Specialized;
using System.Windows.Input;
using Microsoft.Win32;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.Commands.SettingOptions
{
    public class SelectOutputFileCommand : HotKeyCommand, ICommand
    {
        private StringCollection formatFilter;

        public SelectOutputFileCommand(StringCollection filter)
        {
            Text = Lang.SelectOutputFile;
            formatFilter = filter;
        }

        public SelectOutputFileCommand() : this(SettingsHelper.GetJokerFormatFilter())
        {
        }

        public Action<string> AfterExecuted { get; set; }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.CheckPathExists = true;
            saveFileDialog.AddExtension = true;
            saveFileDialog.OverwritePrompt = true;
            saveFileDialog.FileName = SettingsHelper.GetDefaultFileName();
            saveFileDialog.DereferenceLinks = true;
            saveFileDialog.Title = Lang.SelectOutputFile;
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyComputer);
            saveFileDialog.Filter = FileDialogHelper.CreateFormatsFilter(formatFilter);

            bool? userDidChooseFile = saveFileDialog.ShowDialog();
            if (userDidChooseFile == true)
                if (AfterExecuted != null)
                    AfterExecuted.Invoke(saveFileDialog.FileName);
        }
    }
}