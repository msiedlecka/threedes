﻿using System;
using System.Windows.Input;
using ThreeDES.Languages;
using ThreeDES.TaskState;

namespace ThreeDES.Commands
{
    public class CancelCryptoTaskCommand : HotKeyCommand, ICommand
    {
        private uint id;

        public CancelCryptoTaskCommand(uint id)
        {
            this.id = id;
            Text = Lang.Cancel;
        }

        public bool CanExecute(object parameter)
        {
            return true; // this object lives as long as a task does
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            CryptoTasksManager.Instance.CancelTask(id);
        }
    }
}