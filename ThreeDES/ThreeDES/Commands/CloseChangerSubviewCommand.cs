﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;

namespace ThreeDES.Commands
{
    public class CloseChangerSubviewCommand : HotKeyCommand, ICommand
    {
        public Action BeforeExecuted = null;

        private CancelEventHandler closingHandlerToDetach;

        public CloseChangerSubviewCommand(string text, CancelEventHandler handlerToDetach)
        {
            Text = text;
            closingHandlerToDetach = handlerToDetach;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (BeforeExecuted != null)
                BeforeExecuted.Invoke(); // if closing with 'OK' button, the change is saved

            Window beingClosed = parameter as Window;
            if (beingClosed != null)
            {
                beingClosed.Closing -= closingHandlerToDetach;
                beingClosed.Close();
            }
        }
    }
}