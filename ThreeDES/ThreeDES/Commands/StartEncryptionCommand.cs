﻿using System;
using System.Windows.Input;
using ThreeDES.Cryptography.Encryption;
using ThreeDES.Languages;
using ThreeDES.TaskState;

namespace ThreeDES.Commands
{
    public class StartEncryptionCommand : HotKeyCommand, ICommand
    {
        public Func<EncryptionData> BeforeExecute;

        public StartEncryptionCommand()
        {
            Text = Lang.StartEncryption;
            Gesture = new KeyGesture(Key.E, ModifierKeys.Control);
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            if (BeforeExecute != null)
            {
                EncryptionData data = BeforeExecute.Invoke();
                // if data is null, then there must be something missing; 
                // EncryptionViewModel knows what's wrong and reports it to the user
                if (data != null)
                    CryptoTasksManager.Instance.AddTask(data);
            }
        }
    }
}