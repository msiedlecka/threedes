﻿using System;
using System.Windows;
using System.Windows.Input;
using ThreeDES.Languages;

namespace ThreeDES.Commands
{
    public class ShowErrorsCommand : HotKeyCommand, ICommand
    {
        private string errorMessages;

        public ShowErrorsCommand(string errorMessages)
        {
            Text = Lang.ShowErrors;
            this.errorMessages = errorMessages;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            MessageBox.Show(errorMessages, Lang.Error, MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}