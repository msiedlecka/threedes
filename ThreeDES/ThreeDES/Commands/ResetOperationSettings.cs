﻿using System;
using System.Windows.Input;
using ThreeDES.Languages;
using ThreeDES.ViewModels;

namespace ThreeDES.Commands
{
    public class ResetOperationSettings<T> : HotKeyCommand, ICommand where T : IResetable
    {
        public Func<T> BeforeExecute;

        public ResetOperationSettings()
        {
            Text = Lang.RestoreDefaults;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }

        public void Execute(object parameter)
        {
            T viewModel;
            if (BeforeExecute != null)
            {
                viewModel = BeforeExecute.Invoke();
                viewModel.Reset(parameter, true);
            }
        }
    }
}