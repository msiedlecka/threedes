﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using CommonControls;
using ThreeDES.Commands;
using ThreeDES.Commands.SettingOptions;
using ThreeDES.Cryptography;
using ThreeDES.Cryptography.Data;
using ThreeDES.Cryptography.Encryption;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.ViewModels
{
    public class EncryptionViewModel : OperationViewModelBase
    {
        #region Constructors

        public EncryptionViewModel()
        {
            PrepareAvailableModes();
            PrepareSegmentLength();
            PrepareAvailableRecipients();
            PrepareMoveOnListCommands();
            PrepareRemoveRecipient();
        }

        #endregion // end of Constructors

        public override void Reset(object parameter, bool totalReset = true)
        {
            base.Reset(parameter, totalReset);
            Mode = availableModes[0];
            SegmentSize = SettingsHelper.GetDefaultSegmentLength();
            if (totalReset)
                UncheckAllRecipients();
        }

        private void UpdateMoveCommands(bool onlyIndex = true)
        {
            MoveUpCommand.SelectedIndex = SelectedRecipient;
            MoveDownCommand.SelectedIndex = SelectedRecipient;

            if (!onlyIndex)
            {
                MoveUpCommand.List = AvailableRecipients;
                MoveDownCommand.List = AvailableRecipients;
                MoveCheckedToTopCommand.List = AvailableRecipients;
            }
        }

        private void UpdateRemoveRecipient(bool onlyIndex = true)
        {
            RemoveRecipientCommand.SelectedIndex = SelectedRecipient;
            if (!onlyIndex)
                RemoveRecipientCommand.Collection = AvailableRecipients;
        }

        #region Fields

        private List<CipherMode> availableModes;
        private List<int> availableSegmentSizes;
        private CipherMode selectedMode;
        private ObservableCollection<CheckBoxItem<Recipient>> availableRecipients;
        private int selectedRecipient;
        private int segmentSize;

        #endregion // end of Fields

        #region Fields Preparation

        private void PrepareAvailableModes()
        {
            availableModes = new List<CipherMode>(4) {CipherMode.CBC, CipherMode.ECB, CipherMode.CFB, CipherMode.OFB};
            selectedMode = availableModes[0];
        }

        private void PrepareSegmentLength()
        {
            CipherEngineBase des3 = new TripleDesEngine();
            availableSegmentSizes = new List<int>(8);

            int maxSegmentLength = des3.MaxSegmentLength;
            for (int segmentSize = 8; segmentSize <= maxSegmentLength; segmentSize += 8)
                availableSegmentSizes.Add(segmentSize);

            SegmentSize = SettingsHelper.GetDefaultSegmentLength();
        }

        private void PrepareAvailableRecipients()
        {
            availableRecipients = new ObservableCollection<CheckBoxItem<Recipient>>();
            PropagateRecipientsWithCerts(GetCertificatesFromKeysLocations());
        }

        protected override void PrepareKeysLocations()
        {
            KeysFoldersPaths = SettingsHelper.GetPublicKeysPaths();
            KeysStores = SettingsHelper.GetPublicKeysStores();
        }

        #endregion // end of Fields Preparation

        #region Properties

        public override string OperationName
        {
            get { return Lang.Encryption; }
        }

        protected override void InitialiseOutputFileCommand()
        {
            SelectOutputCommand = new SelectOutputFileCommand(SettingsHelper.GetFormatFilterForEncrypted());
        }

        public List<CipherMode> AvailableModes
        {
            get { return availableModes; }
            set
            {
                availableModes = value;
                RaisePropertyChanged("AvailableModes");
            }
        }

        public CipherMode Mode
        {
            get { return selectedMode; }
            set
            {
                selectedMode = value;
                RaisePropertyChanged("Mode");
                RaisePropertyChanged("IsSegmentSizeUsed");
            }
        }

        public ObservableCollection<CheckBoxItem<Recipient>> AvailableRecipients
        {
            get { return availableRecipients; }
            set
            {
                availableRecipients = value;
                UpdateMoveCommands(false);
                UpdateRemoveRecipient(false);
                RaisePropertyChanged("AvailableRecipients");
            }
        }

        public int SelectedRecipient
        {
            get { return selectedRecipient; }
            set
            {
                selectedRecipient = value;
                UpdateMoveCommands();
                UpdateRemoveRecipient();
                RaisePropertyChanged("SelectedRecipient");
            }
        }

        public List<int> AvailableSegmentSizes
        {
            get { return availableSegmentSizes; }
            set
            {
                availableSegmentSizes = value;
                RaisePropertyChanged("AvailableSegmentSizes");
            }
        }

        public int SegmentSize
        {
            get { return segmentSize; }
            set
            {
                segmentSize = value;
                RaisePropertyChanged("SegmentSize");
            }
        }

        public bool IsSegmentSizeUsed
        {
            get { return CipherEngineBase.IsSegmentSizeUsedForMode(Mode); }
        }

        #endregion // end of Properties

        #region Helpers

        public void RefreshRecipients()
        {
            PrepareKeysLocations();
            PropagateRecipientsWithCerts(GetCertificatesFromKeysLocations());
        }

        private X509Certificate2Collection GetCertificatesFromKeysLocations()
        {
            X509Certificate2Collection certificates = CertificateHelper.GetCertificatesFromLocations(KeysFoldersPaths,
                false);
            certificates.AddRange(CertificateHelper.GetCertificatesFromStores(KeysStores));
            return certificates;
        }

        /* with recipients duplication check */

        private void PropagateRecipientsWithCerts(X509Certificate2Collection certificates)
        {
            CheckBoxItem<Recipient> checkItem;
            foreach (X509Certificate2 cert in certificates)
            {
                checkItem = new CheckBoxItem<Recipient>(new Recipient(cert));
                if (!AvailableRecipients.Contains(checkItem))
                    AvailableRecipients.Add(checkItem);
            }
        }

        private List<Recipient> GetChosenRecipients()
        {
            List<Recipient> chosen = new List<Recipient>();
            foreach (CheckBoxItem<Recipient> recipient in AvailableRecipients)
                if (recipient.IsChecked)
                    chosen.Add(recipient.Item);

            return chosen;
        }

        private void UncheckAllRecipients()
        {
            foreach (CheckBoxItem<Recipient> recipient in AvailableRecipients)
                recipient.IsChecked = false;
        }

        private EncryptionData CollectEncryptionData()
        {
            EncryptionData data = new EncryptionData();
            data.InputPath = string.Copy(InputPath);
            data.OutputPath = string.Copy(OutputPath);
            data.CipherData = new CipherData(Mode, SegmentSize);
            data.Recipients = GetChosenRecipients();
            return data;
        }

        /* returns true when validation succeeds (no errors) */

        private bool ValidateEncryptionData(EncryptionData data)
        {
            bool hasErrors = false;
            StringBuilder errors = new StringBuilder();
            string chooseAtLeastOne = "Choose at least one recipient from the list. ";

            hasErrors = ValidateOperationDataIO(data, errors);

            if (data.Recipients.Count == 0)
            {
                hasErrors = true;
                errors.AppendLine(chooseAtLeastOne);
            }

            if (hasErrors)
                ShowErrors(errors, "encryption");

            return !hasErrors;
        }

        #endregion // end of Helpers

        #region Commands

        public StartEncryptionCommand StartCommand { get; set; }

        public ResetOperationSettings<EncryptionViewModel> RestoreDefaultsCommand { get; set; }

        public MoveOnListCommand MoveUpCommand { get; set; }

        public MoveOnListCommand MoveDownCommand { get; set; }

        public MoveCheckedUp MoveCheckedToTopCommand { get; set; }

        public RemoveObjectCommand<CheckBoxItem<Recipient>> RemoveRecipientCommand { get; set; }

        #endregion // end of Commands

        #region Command Preparation

        protected override void PrepareStart()
        {
            StartCommand = new StartEncryptionCommand();
            StartCommand.BeforeExecute += () =>
            {
                EncryptionData data = CollectEncryptionData();
                if (ValidateEncryptionData(data))
                {
                    OutputPath = "";
                    return data;
                }
                return null;
            };
        }

        protected override void PrepareAddKey()
        {
            AddKeyCommand = new SelectInputFileCommand(SettingsHelper.GetFormatFilterForPublicKeys(), false);
            AddKeyCommand.AfterExecuted += certificatePath =>
            {
                X509Certificate2 cert = new X509Certificate2(certificatePath);
                X509Certificate2Collection certCollection = new X509Certificate2Collection(cert);
                PropagateRecipientsWithCerts(certCollection);
            };
        }

        protected override void PrepareImport()
        {
            ImportCertificatesCommand = new ImportFromFolderCommand(false);
            ImportCertificatesCommand.AfterExecuted += listOfCertificatePaths =>
            {
                X509Certificate2Collection certificatesFromPaths =
                    CertificateHelper.GetCertificatesFromPaths(listOfCertificatePaths);
                foreach (X509Certificate2 certificate in certificatesFromPaths)
                    AvailableRecipients.Add(new CheckBoxItem<Recipient>(new Recipient(certificate)));
            };
        }

        protected override void PrepareRestoreDefaults()
        {
            RestoreDefaultsCommand = new ResetOperationSettings<EncryptionViewModel>();
            RestoreDefaultsCommand.BeforeExecute += () => { return this; };
        }

        // two methods below MUST be called after PrepareAvailableRecipients, because AvailableRecipients collection 
        // is passed to RemoveRecipientCommand and MoveCommands' family constructors
        private void PrepareMoveOnListCommands()
        {
            MoveUpCommand = new MoveOnListCommand(Lang.MoveUp, AvailableRecipients, false);
            MoveDownCommand = new MoveOnListCommand(Lang.MoveDown, AvailableRecipients, true);

            MoveCheckedToTopCommand = new MoveCheckedUp(Lang.MoveCheckedUp, AvailableRecipients);

            // instructions below make the selected item point the recently moved item
            MoveUpCommand.AfterExecuted += () => SelectedRecipient--;
            MoveDownCommand.AfterExecuted += () => SelectedRecipient++;
        }

        private void PrepareRemoveRecipient()
        {
            RemoveRecipientCommand = new RemoveObjectCommand<CheckBoxItem<Recipient>>(Lang.RemoveRecipient,
                AvailableRecipients);
        }

        #endregion // end of Command Preparation
    }
}