﻿using System.Collections.ObjectModel;
using System.Globalization;
using ThreeDES.Helpers;

namespace ThreeDES.ViewModels
{
    public class LanguageViewModel : ChangerViewModelBase
    {
        private CultureInfo selectedCulture;

        public LanguageViewModel()
        {
            SelectedCulture = SettingsHelper.GetCurrentCulture();
            OkCommand.BeforeExecuted += () =>
            {
                SettingsHelper.SetCurrentCulture(SelectedCulture);
                SettingsHelper.SaveSettings();
            };
        }

        public CultureInfo SelectedCulture
        {
            get { return selectedCulture; }
            set
            {
                selectedCulture = value;
                RaisePropertyChanged("SelectedCulture");
            }
        }

        public ObservableCollection<CultureInfo> AvailableCultures
        {
            get { return SettingsHelper.GetSupportedCultures(); }
        }

        protected override void CancelChanges()
        {
        }
    }
}