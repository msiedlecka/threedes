﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using CommonControls;
using ThreeDES.Commands;
using ThreeDES.Commands.SettingOptions;
using ThreeDES.Cryptography.Data;
using ThreeDES.Cryptography.Decryption;
using ThreeDES.FileHandlers;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.ViewModels
{
    public class DecryptionViewModel : OperationViewModelBase
    {
        private static string NoMatchingKeyForRecipient =
            "Selected file doesn't contain the private key or the key is not intended to use by recipient: {0}";

        #region Constructors

        public DecryptionViewModel()
        {
            InitialiseDecryptionData();
        }

        #endregion // end of Constructors

        public override void Reset(object parameter, bool totalReset = true)
        {
            base.Reset(parameter, totalReset);
            Recipients = null;
            SwitchablePasswordBox passwordBox = parameter as SwitchablePasswordBox;
            if (passwordBox != null)
                passwordBox.PasswordPlainText = "";
        }

        #region Fields

        private ObservableCollection<Recipient> recipients;
        private Recipient selectedRecipient;
        private DecryptionData data;

        #endregion // end of Fields

        #region Fields Preparation

        private void InitialiseDecryptionData()
        {
            data = new DecryptionData();
        }

        protected override void InitialiseInputFileCommand()
        {
            SelectInputCommand = new SelectInputFileCommand(SettingsHelper.GetFormatFilterForEncrypted());
        }

        protected override void ProcessInputDependentInfo()
        {
            IEncryptedFileParser parser = FileParserFactory.CreateParser(InputPath);
            if (parser != null)
            {
                EncryptedFileHeader header = parser.ParseHeader();
                if (header != null)
                {
                    CertificateHelper.MatchCertificatesToRecipients(
                        CertificateHelper.GetCertificatesPathsFromLocations(KeysFoldersPaths, true),
                        header.ApprovedRecipients);
                    Recipients = new ObservableCollection<Recipient>(header.ApprovedRecipients);
                    data.InitWith(header);
                    data.InputPath = string.Copy(InputPath);
                }
            }
            else
                // if the parser is null, then the extension in the InputPath must be incorrect - so clean the whole path
            {
                data.Clear();
                InputPath = data.InputPath;
            }
        }

        protected override void PrepareKeysLocations()
        {
            KeysFoldersPaths = SettingsHelper.GetPrivateKeysPaths();
            KeysStores = SettingsHelper.GetPrivateKeysStores();
        }

        #endregion // end of Fields Preparation

        #region Properties

        public override string OperationName
        {
            get { return Lang.Decryption; }
        }

        public ObservableCollection<Recipient> Recipients
        {
            get { return recipients; }
            set
            {
                recipients = value;
                RaisePropertyChanged("Recipients");
            }
        }

        public Recipient SelectedRecipient
        {
            get { return selectedRecipient; }
            set
            {
                selectedRecipient = value;
                RaisePropertyChanged("SelectedRecipient");
            }
        }

        #endregion // end of Properties

        #region Helpers

        /* reads again certificates from default locations */

        public void RefreshRecipientsMatching()
        {
            PrepareKeysLocations();
            if (Recipients != null)
            {
                List<Recipient> recipientsList = Recipients.ToList();
                CertificateHelper.MatchCertificatesToRecipients(
                    CertificateHelper.GetCertificatesPathsFromLocations(KeysFoldersPaths, true),
                    recipientsList);
                Recipients = new ObservableCollection<Recipient>(recipientsList);
            }
        }

        private void SupplyDecryptionData()
        {
            data.OutputPath = string.Copy(OutputPath);
            data.Recipient = SelectedRecipient;
        }

        /* returns true when validation succeeds (no errors) */

        private bool ValidateDecryptionData(DecryptionData data)
        {
            bool hasErrors = false;
            StringBuilder errors = new StringBuilder();
            string chooseExactlyOne = "Choose exactly one recipient from the list. ";

            hasErrors = ValidateOperationDataIO(data, errors);

            if (data.Recipient == null)
            {
                hasErrors = true;
                errors.AppendLine(chooseExactlyOne);
            }

            if (hasErrors)
                ShowErrors(errors, "decryption");

            return !hasErrors;
        }

        private void AppendNewRecipients(ICollection<string> certificatePaths, bool standardVersion = false)
        {
            foreach (string path in certificatePaths)
                if (CertificateHelper.ContainsPrivateKey(path, standardVersion))
                    Recipients.Add(new Recipient(path));
        }

        #endregion // end of Helpers

        #region Commands

        public StartDecryptionCommand StartCommand { get; set; }

        public ResetOperationSettings<DecryptionViewModel> RestoreDefaultsCommand { get; set; }

        #endregion // end of Commands

        #region Command Preparation

        protected override void PrepareStart()
        {
            StartCommand = new StartDecryptionCommand();
            StartCommand.BeforeExecute += () =>
            {
                SupplyDecryptionData();
                if (ValidateDecryptionData(data))
                {
                    OutputPath = "";
                    return data;
                }
                return null;
            };
        }

        protected override void PrepareRestoreDefaults()
        {
            RestoreDefaultsCommand = new ResetOperationSettings<DecryptionViewModel>();
            RestoreDefaultsCommand.BeforeExecute += () => { return this; };
        }

        protected override void PrepareAddKey()
        {
            AddKeyCommand = new SelectInputFileCommand(SettingsHelper.GetFormatFilterForPrivateKeys(), false);
            AddKeyCommand.AfterExecuted += privateKeyPath =>
            {
                if (!CertificateHelper.CheckRecipientMatch(privateKeyPath, selectedRecipient, true))
                    MessageBox.Show(string.Format(NoMatchingKeyForRecipient, selectedRecipient.Label),
                        Lang.Error, MessageBoxButton.OK, MessageBoxImage.Warning);
            };
        }

        protected override void PrepareImport()
        {
            ImportCertificatesCommand = new ImportFromFolderCommand(true);
            ImportCertificatesCommand.AfterExecuted +=
                listOfCertificatePaths =>
                {
                    CertificateHelper.MatchCertificatesToRecipients(listOfCertificatePaths, Recipients);
                };
        }

        #endregion // end of Command Preparation
    }
}