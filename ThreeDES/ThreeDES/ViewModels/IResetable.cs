﻿namespace ThreeDES.ViewModels
{
    public interface IResetable
    {
        void Reset(object parameter, bool totalReset = true);
    }
}