﻿using System;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using CommonControls;
using ThreeDES.Commands;
using ThreeDES.Commands.SettingOptions;
using ThreeDES.Languages;
using ThreeDES.TaskState;
using ThreeDES.Views;

namespace ThreeDES.ViewModels
{
    public class MainViewModel : PropertyChangedNotifier
    {
        public MainViewModel()
        {
            PrepareClose();
            PrepareChangeLanguage();
            PrepareAbout();
            PrepareSelectKeyLocations();
            PrepareEncryptionInfo();
            PrepareDecryptionInfo();
        }

        public event EventHandler RequestClose;

        public void OnMainWindowClosing(object sender, CancelEventArgs e)
        {
            Action afterCloseExecute = CloseCommand.AfterExecuted;
            CloseCommand.AfterExecuted = null;
            CloseCommand.Execute(new object());
            if (CloseCommand.Cancelled)
            {
                CloseCommand.AfterExecuted = afterCloseExecute;
                e.Cancel = true;
            }
        }

        #region Commands

        public CloseCommand CloseCommand { get; set; }
        public ShowAboutCommand AboutCommand { get; set; }
        public ChangeLanguageCommand ChangeLanguageCommand { get; set; }
        public SelectKeysLocationsCommand SelectKeyLocationsCommand { get; set; }

        #endregion // end of Commands

        #region Command Preparation

        private void PrepareClose()
        {
            CloseCommand = new CloseCommand();
            CloseCommand.BeforeExecute = () =>
            {
                if (CryptoTasksManager.Instance.AreAnyTasksInProgress())
                {
                    MessageBoxResult result = MessageBox.Show(Lang.QCancelActionsPending, Lang.Close,
                        MessageBoxButton.YesNo, MessageBoxImage.Question);
                    if (result.Equals(MessageBoxResult.No))
                    {
                        CloseCommand.Cancelled = true; // program keeps working
                    }
                    else
                    {
                        BusyIndicatorView busyView = new BusyIndicatorView();
                        busyView.Show();
                        Dispatcher mainDispatcher = Dispatcher.CurrentDispatcher;
                        Task.Factory.StartNew(() =>
                        {
                            CryptoTasksManager.Instance.CancelAllTasks();
                            mainDispatcher.Invoke((Action) (() => busyView.Close()), null);
                        });
                        CloseCommand.Cancelled = false;
                    }
                }
            };
            CloseCommand.AfterExecuted = () =>
            {
                if (RequestClose != null)
                    RequestClose.Invoke(this, EventArgs.Empty);
            };
        }

        private void PrepareChangeLanguage()
        {
            ChangeLanguageCommand = new ChangeLanguageCommand();
        }

        private void PrepareSelectKeyLocations()
        {
            SelectKeyLocationsCommand = new SelectKeysLocationsCommand();
            SelectKeyLocationsCommand.AfterExecuted += () =>
            {
                EncryptionInformation.RefreshRecipients();
                DecryptionInformation.RefreshRecipientsMatching();
            };
        }

        private void PrepareAbout()
        {
            AboutCommand = new ShowAboutCommand();
        }

        #endregion // end of Command Preparation

        #region Fields

        private EncryptionViewModel encryptionInformation;
        private DecryptionViewModel decryptionInformation;

        #endregion // end of Fields

        #region Fields Preparation

        private void PrepareEncryptionInfo()
        {
            encryptionInformation = new EncryptionViewModel();
        }

        private void PrepareDecryptionInfo()
        {
            decryptionInformation = new DecryptionViewModel();
        }

        #endregion // end of Fields Preparation

        #region Properties

        public EncryptionViewModel EncryptionInformation
        {
            get { return encryptionInformation; }
            set
            {
                encryptionInformation = value;
                RaisePropertyChanged("EncryptionInformation");
            }
        }

        public DecryptionViewModel DecryptionInformation
        {
            get { return decryptionInformation; }
            set
            {
                decryptionInformation = value;
                RaisePropertyChanged("DecryptionInformation");
            }
        }

        #endregion // end of Properties
    }
}