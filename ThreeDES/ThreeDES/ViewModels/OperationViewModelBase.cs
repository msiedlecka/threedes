﻿using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows;
using CommonControls;
using ThreeDES.Commands.SettingOptions;
using ThreeDES.Cryptography;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.ViewModels
{
    public abstract class OperationViewModelBase : PropertyChangedNotifier, IInputOutputInfo, IResetable
    {
        #region Constructors

        public OperationViewModelBase()
        {
            PrepareIO();
            PrepareKeysLocations();
            PrepareStart();
            PrepareAddKey();
            PrepareImport();
            PrepareRestoreDefaults();
        }

        #endregion // end of Constructors

        public virtual void Reset(object parameter, bool totalReset = true)
        {
            InputPath = "";
            if (totalReset)
                OutputPath = "";
        }

        #region Fields

        protected string inputPath;
        protected string outputPath;
        protected ObservableCollection<string> keysFoldersPaths;
        protected ObservableCollection<X509Store> keysStores;

        private static string pathToNonexisting = "Path \"{0}\" points to nonexisting file. ";
        private static string provideValid = "Please provide a valid one. ";

        #endregion // end of Fields

        #region Fields Preparation

        protected void PrepareIO()
        {
            InputPath = "";
            OutputPath = "";
            InitialiseInputFileCommand();
            InitialiseOutputFileCommand();
            SelectInputCommand.AfterExecuted += chosenInputFile => { InputPath = chosenInputFile; };
            SelectOutputCommand.AfterExecuted += chosenOutputFile => { OutputPath = chosenOutputFile; };
        }

        protected virtual void InitialiseInputFileCommand()
        {
            SelectInputCommand = new SelectInputFileCommand();
        }

        protected virtual void InitialiseOutputFileCommand()
        {
            SelectOutputCommand = new SelectOutputFileCommand();
        }

        protected virtual void CheckAndProcessInputDependentInfo()
        {
            if (InputPath != null && InputPath != "")
            {
                string absolutePath = FileDialogHelper.ToAbsolutePath(InputPath);
                if (File.Exists(absolutePath))
                {
                    ProcessInputDependentInfo();
                }
                else
                {
                    string message = string.Format(pathToNonexisting, absolutePath) + provideValid;
                    MessageBox.Show(message, Lang.Error, MessageBoxButton.OK, MessageBoxImage.Exclamation);
                    Reset(null, false);
                }
            }
        }

        protected virtual void ProcessInputDependentInfo()
        {
        }

        protected abstract void PrepareKeysLocations();

        protected abstract void PrepareStart(); // preparation of commands to start encryption/decryption

        protected abstract void PrepareAddKey();

        protected abstract void PrepareImport();

        protected abstract void PrepareRestoreDefaults();

        #endregion // end of Fields Preparation

        #region Properties

        public abstract string OperationName { get; }

        public string InputPath
        {
            get { return inputPath; }
            set
            {
                inputPath = value;
                RaisePropertyChanged("InputPath");
                CheckAndProcessInputDependentInfo();
            }
        }

        public string OutputPath
        {
            get { return outputPath; }
            set
            {
                outputPath = value;
                RaisePropertyChanged("OutputPath");
            }
        }

        public ObservableCollection<string> KeysFoldersPaths
        {
            get { return keysFoldersPaths; }
            set
            {
                keysFoldersPaths = value;
                RaisePropertyChanged("KeysFoldersPaths");
            }
        }

        public ObservableCollection<X509Store> KeysStores
        {
            get { return keysStores; }
            set
            {
                keysStores = value;
                RaisePropertyChanged("KeysStores");
            }
        }

        #endregion // end of Properties

        #region Operation Data Basic Validation

        protected bool ValidateOperationDataIO(IInputOutputInfo data, StringBuilder errors)
        {
            bool hasErrors = false;
            string pathIsMissing = "Path to {0} file is missing. ";
            string nonExisitingDirectory = "Directory {0} doesn't exist. ";
            string notExisitingOutputDirectory = "Directory of provided output file path ({0}) doesn't exist.";
            string doWantCreateDirectory = "Do you want to the application to create it for you? ";
            string pointToTheSame =
                "Input and output paths point to the same file ({0}). Change one of them and try again.";

            if (data.InputPath == null || data.InputPath == "")
            {
                hasErrors = true;
                errors.AppendFormat(pathIsMissing, "input");
                errors.AppendLine(provideValid);
            }
            else if (!File.Exists(FileDialogHelper.ToAbsolutePath(data.InputPath)))
            {
                hasErrors = true;
                errors.AppendFormat(pathToNonexisting, FileDialogHelper.ToAbsolutePath(data.InputPath));
                errors.AppendLine(provideValid);
            }

            if (data.OutputPath == null || data.OutputPath == "")
            {
                hasErrors = true;
                errors.AppendFormat(pathIsMissing, "output");
                errors.AppendLine(provideValid);
            }
            else if (!FileDialogHelper.IsExisitingPath(data.OutputPath))
            {
                MessageBoxResult result = MessageBox.Show(string.Format(nonExisitingDirectory,
                    Path.GetDirectoryName(FileDialogHelper.ToAbsolutePath(data.OutputPath)) + doWantCreateDirectory,
                    Lang.Information, MessageBoxButton.YesNo, MessageBoxImage.Hand));

                if (result.Equals(MessageBoxResult.Yes))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(FileDialogHelper.ToAbsolutePath(data.OutputPath)));
                }
                else
                {
                    hasErrors = true;
                    errors.AppendFormat(notExisitingOutputDirectory, FileDialogHelper.ToAbsolutePath(data.OutputPath));
                    errors.AppendLine(provideValid);
                }
            }

            if (!hasErrors &&
                FileDialogHelper.ToAbsolutePath(data.InputPath).Equals(FileDialogHelper.ToAbsolutePath(data.OutputPath)))
            {
                hasErrors = true;
                errors.AppendFormat(pointToTheSame, FileDialogHelper.ToAbsolutePath(data.InputPath));
            }

            return hasErrors;
        }

        protected void ShowErrors(StringBuilder errors, string operationName)
        {
            MessageBox.Show(
                string.Format(
                    "There is not enough information provided for {0} to start or some information is incorrect. See messages below.{1}{2}",
                    operationName, Environment.NewLine, errors), Lang.NotEnoughInfo, MessageBoxButton.OK,
                MessageBoxImage.Exclamation);
        }

        #endregion // end of Operation Data Basic Validation

        #region Commands

        public SelectInputFileCommand SelectInputCommand { get; protected set; }

        public SelectOutputFileCommand SelectOutputCommand { get; protected set; }

        public SelectInputFileCommand AddKeyCommand { get; protected set; }

        public ImportFromFolderCommand ImportCertificatesCommand { get; set; }

        #endregion // end of Commands
    }
}