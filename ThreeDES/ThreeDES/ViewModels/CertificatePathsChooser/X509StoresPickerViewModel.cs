﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CommonControls;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.ViewModels.CertificatePathsChooser
{
    public class X509StoresPickerViewModel : PropertyChangedNotifier
    {
        private ObservableCollection<CheckBoxItem<X509StoreIdentity>> availableStores;
        private bool forLocalMachine;
        private bool forPrivateKeys;
        private string header;

        public X509StoresPickerViewModel(bool forLocalMachine = false, bool forPrivateKeys = false)
        {
            this.forLocalMachine = forLocalMachine;
            this.forPrivateKeys = forPrivateKeys;
            ListHeader = string.Format("{0}:", forLocalMachine ? Lang.LocalMachine : Lang.CurrentUser);
            PrepareItems(forLocalMachine, forPrivateKeys);
        }

        public ObservableCollection<CheckBoxItem<X509StoreIdentity>> Items
        {
            get { return availableStores; }
            set
            {
                availableStores = value;
                RaisePropertyChanged("Items");
            }
        }

        public string ListHeader
        {
            get { return header; }
            set
            {
                header = value;
                RaisePropertyChanged("ListHeader");
            }
        }

        public void SaveChanges()
        {
            SettingsHelper.SetKeysStores(CollectCheckedStores(), forLocalMachine, forPrivateKeys);
        }

        private StringCollection CollectCheckedStores()
        {
            StringCollection storeNamesCollection = new StringCollection();
            foreach (CheckBoxItem<X509StoreIdentity> id in Items)
                if (id.IsChecked)
                    storeNamesCollection.Add(id.Item.StoreName);
            return storeNamesCollection;
        }

        private void PrepareItems(bool forLocalMachine = false, bool forPrivateKeys = false)
        {
            ObservableCollection<X509Store> savedStores = SettingsHelper.GetKeysStores(forLocalMachine, forPrivateKeys);
            StoreLocation storeLocation = forLocalMachine ? StoreLocation.LocalMachine : StoreLocation.CurrentUser;
            string locationName = Enum.GetName(typeof(StoreLocation), storeLocation);
            string[] allStoreNames = Enum.GetNames(typeof(StoreName));
            bool isChosen = false;
            X509Store comparingStore;

            Items = new ObservableCollection<CheckBoxItem<X509StoreIdentity>>();

            savedStores = SettingsHelper.GetKeysStores(forLocalMachine, forPrivateKeys);

            foreach (string storeName in allStoreNames)
            {
                comparingStore = new X509Store(storeName, storeLocation);
                if (CertificateHelper.IsStoreAvailable(comparingStore))
                {
                    isChosen =
                        savedStores.Count(
                            store => { return store.Name.Equals(storeName) && store.Location.Equals(storeLocation); }) >
                        0;
                    Items.Add(new CheckBoxItem<X509StoreIdentity>(new X509StoreIdentity(storeName, locationName),
                        isChosen));
                }
            }
        }
    }
}