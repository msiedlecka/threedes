﻿using CommonControls;

namespace ThreeDES.ViewModels.CertificatePathsChooser
{
    public class CertPathsChooserViewModel : PropertyChangedNotifier
    {
        public CertPathsChooserViewModel(bool forPrivateKeys = false)
        {
            PrepareAdderRemover(forPrivateKeys);
            PrepareStorePicker(forPrivateKeys);
        }

        public PathsAdderRemoverViewModel AdderRemover { get; set; }

        public TotalX509StoresPickerViewModel StorePicker { get; set; }

        private void PrepareAdderRemover(bool forPrivateKeys = false)
        {
            AdderRemover = new PathsAdderRemoverViewModel(forPrivateKeys);
        }

        private void PrepareStorePicker(bool forPrivateKeys = false)
        {
            StorePicker = new TotalX509StoresPickerViewModel(forPrivateKeys);
        }

        public void SaveChanges()
        {
            AdderRemover.SaveChanges();
            StorePicker.SaveChanges();
        }
    }
}