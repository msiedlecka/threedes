﻿using CommonControls;
using ThreeDES.Languages;

namespace ThreeDES.ViewModels.CertificatePathsChooser
{
    public class TotalX509StoresPickerViewModel : PropertyChangedNotifier
    {
        private string groupHeader;
        private X509StoresPickerViewModel left, right;

        public TotalX509StoresPickerViewModel(bool forPrivateKeys = false)
        {
            GroupHeader = Lang.CertificateStores;
            Left = new X509StoresPickerViewModel(false, forPrivateKeys);
            Right = new X509StoresPickerViewModel(true, forPrivateKeys);
        }

        public string GroupHeader
        {
            get { return groupHeader; }
            set
            {
                groupHeader = value;
                RaisePropertyChanged("GroupHeader");
            }
        }

        public X509StoresPickerViewModel Left
        {
            get { return left; }
            set
            {
                left = value;
                RaisePropertyChanged("Left");
            }
        }

        public X509StoresPickerViewModel Right
        {
            get { return right; }
            set
            {
                right = value;
                RaisePropertyChanged("Right");
            }
        }

        public void SaveChanges()
        {
            Left.SaveChanges();
            Right.SaveChanges();
        }
    }
}