﻿using ThreeDES.Helpers;

namespace ThreeDES.ViewModels.CertificatePathsChooser
{
    public class KeyLocationsViewModel : ChangerViewModelBase
    {
        public KeyLocationsViewModel()
        {
            PrepareSources();
            OkCommand.BeforeExecuted += () => { SaveChanges(); };
        }

        public CertPathsChooserViewModel PublicSources { get; set; }

        public CertPathsChooserViewModel PrivateSources { get; set; }

        protected override void CancelChanges()
        {
        }

        private void PrepareSources()
        {
            PublicSources = new CertPathsChooserViewModel(false);
            PrivateSources = new CertPathsChooserViewModel(true);
        }

        private void SaveChanges()
        {
            PublicSources.SaveChanges();
            PrivateSources.SaveChanges();
            SettingsHelper.SaveSettings();
        }
    }
}