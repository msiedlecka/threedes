﻿using System.Collections.ObjectModel;
using CommonControls;
using ThreeDES.Commands.SettingOptions;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.ViewModels.CertificatePathsChooser
{
    public class PathsAdderRemoverViewModel : PropertyChangedNotifier
    {
        private ObservableCollection<string> chosenPaths;
        bool forPrivateKeys;
        private string groupHeader;
        private int selectedPathIndex;

        public PathsAdderRemoverViewModel(bool forPrivateKeys = false)
        {
            this.forPrivateKeys = forPrivateKeys;
            PrepareChosenPaths(forPrivateKeys);
            GroupHeader = Lang.ChooseFolder;
            PrepareAddCommand();
            PrepareRemoveCommand();
            PrepareMoveOnListCommands();
        }

        public string GroupHeader
        {
            get { return groupHeader; }
            set
            {
                groupHeader = value;
                RaisePropertyChanged("GroupHeader");
            }
        }

        public ObservableCollection<string> ChosenPaths
        {
            get { return chosenPaths; }
            set
            {
                chosenPaths = value;
                UpdateRemoveCommand(false);
                UpdateMoveCommands(false);
                RaisePropertyChanged("ChosenPaths");
            }
        }

        public int SelectedPathIndex
        {
            get { return selectedPathIndex; }
            set
            {
                selectedPathIndex = value;
                UpdateRemoveCommand();
                UpdateMoveCommands();
                RaisePropertyChanged("SelectedPathIndex");
            }
        }

        public ChooseFolderCommand AddCommand { get; set; }

        public RemoveObjectCommand<string> RemoveCommand { get; set; }

        public MoveOnListCommand MoveUpCommand { get; set; }

        public MoveOnListCommand MoveDownCommand { get; set; }

        public void SaveChanges()
        {
            SettingsHelper.SetKeysPaths(chosenPaths, forPrivateKeys);
        }

        private void PrepareAddCommand()
        {
            AddCommand = new ChooseFolderCommand();
            AddCommand.AfterExecuted += folderPath =>
            {
                if (!ChosenPaths.Contains(folderPath))
                    ChosenPaths.Add(folderPath);
            };
        }

        private void PrepareRemoveCommand()
        {
            RemoveCommand = new RemoveObjectCommand<string>(Lang.RemovePath, ChosenPaths);
        }

        // MUST be called before PrepareRemoveCommand AND PrepareMoveOnListCommands, 
        // because ChosenPaths collection is passed to RemoveCommand's and MoveOnListCommands' constructors
        private void PrepareChosenPaths(bool forPrivateKeys = false)
        {
            chosenPaths = forPrivateKeys ? SettingsHelper.GetPrivateKeysPaths() : SettingsHelper.GetPublicKeysPaths();
        }

        private void PrepareMoveOnListCommands()
        {
            MoveUpCommand = new MoveOnListCommand(Lang.MoveUp, ChosenPaths, false);
            MoveDownCommand = new MoveOnListCommand(Lang.MoveDown, ChosenPaths, true);
            MoveUpCommand.AfterExecuted += () => SelectedPathIndex--;
            MoveDownCommand.AfterExecuted += () => SelectedPathIndex++;
        }

        private void UpdateMoveCommands(bool onlyIndex = true)
        {
            MoveUpCommand.SelectedIndex = SelectedPathIndex;
            MoveDownCommand.SelectedIndex = SelectedPathIndex;

            if (!onlyIndex)
            {
                MoveUpCommand.List = ChosenPaths;
                MoveDownCommand.List = ChosenPaths;
            }
        }

        private void UpdateRemoveCommand(bool onlyIndex = true)
        {
            RemoveCommand.SelectedIndex = SelectedPathIndex;
            if (!onlyIndex)
                RemoveCommand.Collection = ChosenPaths;
        }
    }
}