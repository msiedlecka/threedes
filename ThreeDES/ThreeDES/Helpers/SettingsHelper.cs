﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using ThreeDES.Properties;

namespace ThreeDES.Helpers
{
    public class SettingsHelper
    {
        public static bool UseStandardPrivateKeyFormat()
        {
            return Settings.Default.UseStandardPrivateKeyFormat;
        }

        public static int GetDefaultSegmentLength()
        {
            return Settings.Default.DefaultSegmentLength;
        }

        public static int GetChunkSizeInBytes()
        {
            return Settings.Default.ChunkSizeInBytes;
        }

        public static string GetXmlFixedAlgorithmName()
        {
            return Settings.Default.XmlFixedAlgorithmName;
        }

        public static string GetXmlHeaderName()
        {
            return Settings.Default.XmlHeaderName;
        }

        public static string GetDefaultFileName()
        {
            return Settings.Default.DefaultFileName;
        }

        public static Encoding GetDefaultFileEncoding()
        {
            return Encoding.GetEncoding(Settings.Default.DefaultFileEncoding);
        }

        public static ObservableCollection<string> GetPrivateKeysPaths()
        {
            return StringCollectionToObservable(Settings.Default.PrivateKeysPaths);
        }

        public static ObservableCollection<string> GetPublicKeysPaths()
        {
            return StringCollectionToObservable(Settings.Default.PublicKeysPaths);
        }

        public static void SetKeysPaths(ObservableCollection<string> paths, bool forPrivateKeys = false)
        {
            StringCollection settingsPaths = new StringCollection();
            foreach (string path in paths)
                settingsPaths.Add(path);

            if (forPrivateKeys)
                Settings.Default.PrivateKeysPaths = settingsPaths;
            else
                Settings.Default.PublicKeysPaths = settingsPaths;
        }

        public static ObservableCollection<X509Store> GetPrivateKeysStores()
        {
            return AddStoresFromCUAndLM(Settings.Default.PrivateKeysCUStores,
                Settings.Default.PrivateKeysLMStores);
        }

        public static ObservableCollection<X509Store> GetPublicKeysStores()
        {
            return AddStoresFromCUAndLM(Settings.Default.PublicKeysCUStores,
                Settings.Default.PublicKeysLMStores);
        }

        public static ObservableCollection<X509Store> GetKeysStores(bool forLocalMachine = false,
            bool forPrivateKeys = false)
        {
            ObservableCollection<X509Store> stores = new ObservableCollection<X509Store>();
            StringCollection names = GetStoresNames(forLocalMachine, forPrivateKeys);

            AddValidStoresToCollection(names, forLocalMachine ? StoreLocation.LocalMachine : StoreLocation.CurrentUser,
                ref stores);
            return stores;
        }

        public static void SetKeysStores(StringCollection storeNamesCollection, bool forLocalMachine = false,
            bool forPrivateKeys = false)
        {
            if (forLocalMachine)
            {
                if (forPrivateKeys)
                    Settings.Default.PrivateKeysLMStores = storeNamesCollection;
                else
                    Settings.Default.PublicKeysLMStores = storeNamesCollection;
            }
            else
            {
                if (forPrivateKeys)
                    Settings.Default.PrivateKeysCUStores = storeNamesCollection;
                else
                    Settings.Default.PublicKeysCUStores = storeNamesCollection;
            }
        }

        /* collection of strings containing only extension pattern, eg. "*.pem" */

        public static StringCollection GetPublicKeysExtensions()
        {
            return FileDialogHelper.ExtractOnlyExtenstions(Settings.Default.PublicKeysFormatFilters);
        }

        /* collection of strings containing the patterns for eg. OpenFileDialog, like "PEM files|*.pem" */

        public static StringCollection GetFormatFilterForPublicKeys()
        {
            return Settings.Default.PublicKeysFormatFilters;
        }

        /* collection of strings containing only extension pattern, eg. *.pem */

        public static StringCollection GetPrivateKeysExtensions()
        {
            return FileDialogHelper.ExtractOnlyExtenstions(Settings.Default.PrivateKeysFormatFilters);
        }

        /* collection of strings containing the patterns for eg. OpenFileDialog, like "PEM files|*.pem" */

        public static StringCollection GetFormatFilterForPrivateKeys()
        {
            return Settings.Default.PrivateKeysFormatFilters;
        }

        public static StringCollection GetFormatFilterForEncrypted()
        {
            return Settings.Default.EncryptedFilesFormatFilters;
        }

        public static StringCollection GetJokerFormatFilter()
        {
            /*
             * 1) it's actually not worth to store this string in Settings, because it's a constant value used only here
             * 2) returns StringCollection, because there already exists a method that handles extenstions stored 
             * in StringCollection (so it's more general)
             */
            StringCollection collectionOfOneExtension = new StringCollection();
            collectionOfOneExtension.Add("All files|*.*");
            return collectionOfOneExtension;
        }

        /* merging removes duplicates */

        public static StringCollection Merge(StringCollection first, StringCollection second)
        {
            StringCollection merged = new StringCollection();
            StringCollection mergedNoDuplicates = new StringCollection();
            merged.AddRange(StringCollectionToArray(first));
            merged.AddRange(StringCollectionToArray(second));
            mergedNoDuplicates.AddRange(
                merged.Cast<string>().Distinct(StringComparer.InvariantCultureIgnoreCase).ToArray());
            return mergedNoDuplicates;
        }

        public static CultureInfo GetCurrentCulture()
        {
            return Settings.Default.DefaultCulture;
        }

        public static void SetCurrentCulture(CultureInfo culture)
        {
            Settings.Default.DefaultCulture = culture;
        }

        public static void ActivateCurrentCulture()
        {
            Thread.CurrentThread.CurrentCulture = GetCurrentCulture();
            Thread.CurrentThread.CurrentUICulture = GetCurrentCulture();
        }

        public static void SaveSettings()
        {
            Settings.Default.Save();
        }

        public static ObservableCollection<CultureInfo> GetSupportedCultures()
        {
            StringCollection supportedCulturesList = Settings.Default.SupportedLanguages;
            ObservableCollection<CultureInfo> culturesList = new ObservableCollection<CultureInfo>();
            foreach (string cultureName in supportedCulturesList)
                culturesList.Add(new CultureInfo(cultureName));
            return culturesList;
        }

        #region Private Helpers

        private static StringCollection GetStoresNames(bool forLocalMachine = false, bool forPrivateKeys = false)
        {
            if (forLocalMachine)
                if (forPrivateKeys)
                    return Settings.Default.PrivateKeysLMStores;
                else
                    return Settings.Default.PublicKeysLMStores;
            if (forPrivateKeys)
                return Settings.Default.PrivateKeysCUStores;
            return Settings.Default.PublicKeysCUStores;
        }

        private static ObservableCollection<string> StringCollectionToObservable(StringCollection collection)
        {
            ObservableCollection<string> observableCollection = new ObservableCollection<string>();
            foreach (string path in collection)
                observableCollection.Add(path);
            return observableCollection;
        }

        private static void AddValidStoresToCollection(StringCollection names,
            StoreLocation location, ref ObservableCollection<X509Store> originalStores)
        {
            StoreName sn;
            foreach (string name in names)
                if (Enum.TryParse(name, out sn))
                    originalStores.Add(new X509Store(sn, location));
        }

        /* CU - CurrentUser, LM - Local Machine */

        private static ObservableCollection<X509Store> AddStoresFromCUAndLM(StringCollection cuNames,
            StringCollection lmNames)
        {
            ObservableCollection<X509Store> stores = new ObservableCollection<X509Store>();
            AddValidStoresToCollection(cuNames, StoreLocation.CurrentUser, ref stores);
            AddValidStoresToCollection(lmNames, StoreLocation.LocalMachine, ref stores);
            return stores;
        }

        private static string[] StringCollectionToArray(StringCollection collection)
        {
            string[] collectionAsArray = new string[collection.Count];
            collection.CopyTo(collectionAsArray, 0);
            return collectionAsArray;
        }

        #endregion // end of Private Helpers
    }
}