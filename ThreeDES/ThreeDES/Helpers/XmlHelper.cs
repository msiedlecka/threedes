﻿namespace ThreeDES.Helpers
{
    public abstract class XmlHelper
    {
        public static string CreateEndElement(string elementName)
        {
            return string.Format("</{0}>", elementName);
        }
    }
}