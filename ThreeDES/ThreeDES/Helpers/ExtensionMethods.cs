﻿using System;
using System.Collections;

namespace ExtensionMethods
{
    public static class Extensions
    {
        public static bool InOpenInterval<T>(this T value, T from, T to) where T : IComparable<T>
        {
            return value.CompareTo(from) > 0 && value.CompareTo(to) < 0;
        }

        public static bool InClosedInterval<T>(this T value, T from, T to) where T : IComparable<T>
        {
            return value.CompareTo(from) >= 0 && value.CompareTo(to) <= 0;
        }

        public static void Swap(this IList collection, int first, int second)
        {
            object tmp = collection[first];
            collection[first] = collection[second];
            collection[second] = tmp;
        }
    }
}