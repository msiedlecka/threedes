﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using Ookii.Dialogs.Wpf;

namespace ThreeDES.Helpers
{
    public class FileDialogHelper
    {
        private const string filterDelimiter = "|";

        public static string CreateFormatsFilter(StringCollection extensionsList)
        {
            StringBuilder filterBuilder = new StringBuilder();

            foreach (string extension in extensionsList)
                filterBuilder.Append(extension + filterDelimiter);
            filterBuilder.Remove(filterBuilder.Length - 1, 1); // removes the last, undesirable delimiter

            return filterBuilder.ToString();
        }

        // a default extension is the first at the list of supported extensions
        public static string GetDefaultFormat(StringCollection extensionsList)
        {
            string defaultExtension = extensionsList.Count > 0 ? extensionsList[0] : "";
            return defaultExtension;
        }

        // eg. "PEM file|*.pem" --> "*.pem"
        public static StringCollection ExtractOnlyExtenstions(StringCollection formatFilters)
        {
            StringCollection extensions = new StringCollection();
            string[] filterParts;
            string[] delimiters = {filterDelimiter};

            foreach (string formatFilter in formatFilters)
            {
                filterParts = formatFilter.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);
                if (filterParts.Length > 1)
                    extensions.Add(filterParts[1]);
            }

            return extensions;
        }

        // if no folder chosen, returns null
        public static string ChooseFolder()
        {
            VistaFolderBrowserDialog folderChooser = new VistaFolderBrowserDialog();
            folderChooser.RootFolder = Environment.SpecialFolder.MyComputer;
            folderChooser.ShowNewFolderButton = true;
            folderChooser.UseDescriptionForTitle = false;

            bool? userDidChooseFolder = folderChooser.ShowDialog();
            if (userDidChooseFolder == true)
                return folderChooser.SelectedPath;
            return null;
        }

        public static bool IsExisitingPath(string path)
        {
            path = ToAbsolutePath(path);
            return Directory.Exists(Path.GetDirectoryName(path));
        }

        /* 
         * if a path already is absolute, it's not changed 
         * if path == null, null is returned
         */

        public static string ToAbsolutePath(string path)
        {
            if (path != null && !Path.IsPathRooted(path))
            {
                string currentWorkingAppDirectory = Directory.GetCurrentDirectory();
                string absolutePath = Path.Combine(currentWorkingAppDirectory, path);
                path = absolutePath;
            }

            return path;
        }
    }
}