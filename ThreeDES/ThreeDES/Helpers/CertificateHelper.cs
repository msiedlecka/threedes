﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Windows;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.OpenSsl;
using ThreeDES.Cryptography;
using ThreeDES.Cryptography.Data;
using ThreeDES.Languages;

namespace ThreeDES.Helpers
{
    public abstract class CertificateHelper
    {
        public static string FileIsCorrupted = "The file is probably corrupted.";

        public static X509Certificate2Collection GetCertificatesFromLocations(ICollection<string> locationPaths,
            bool withPrivateKeys = false)
        {
            return GetCertificatesFromPaths(GetCertificatesPathsFromLocations(locationPaths, withPrivateKeys));
        }

        public static X509Certificate2Collection GetCertificatesFromStores(ICollection<X509Store> stores)
        {
            X509Certificate2Collection certificates = new X509Certificate2Collection();
            StringBuilder errors = new StringBuilder();

            foreach (X509Store store in stores)
                try
                {
                    store.Open(OpenFlags.ReadOnly | OpenFlags.OpenExistingOnly);
                    certificates.AddRange(store.Certificates.Find(X509FindType.FindByTimeValid, DateTime.Now, true));
                }
                catch (CryptographicException ce)
                {
                    errors.AppendFormat(
                        "The {0} doesn't exist or is unreadable, or contains protected certificates. {1}{2}{3}{4}.",
                        GetStorePresentationStringForError(store), Environment.NewLine, GetExceptionMessage(ce),
                        ToAvoidInFuture, UncheckStoreHint);
                }
                catch (SecurityException se)
                {
                    errors.AppendFormat(
                        "The application does not have the required permissions to read the {0}.{1}{2}{3}{4}{5}{6}.",
                        GetStorePresentationStringForError(store), Environment.NewLine, GetExceptionMessage(se),
                        ToAvoidInFuture, RunWithHigherPriviledges, " or ", UncheckStoreHint);
                }
                catch (ArgumentException ae)
                {
                    errors.AppendFormat("The {0} contains invalid values.{1}{2}{3}{4}.",
                        GetStorePresentationStringForError(store), Environment.NewLine, GetExceptionMessage(ae),
                        ToAvoidInFuture, UncheckStoreHint);
                }
                catch (Exception ex)
                {
                    errors.AppendFormat("Error reading the {0}.{1}{2}{3}{4}.",
                        GetStorePresentationStringForError(store), Environment.NewLine, GetExceptionMessage(ex),
                        ToAvoidInFuture, UncheckStoreHint);
                }
                finally
                {
                    store.Close();
                }

            if (errors.Length > 0)
                MessageBox.Show(errors.ToString(), Lang.ErrorRetrievingCertificates, MessageBoxButton.OK,
                    MessageBoxImage.Exclamation);

            return certificates;
        }

        public static List<string> GetCertificatesPathsFromLocations(ICollection<string> locationPaths,
            bool withPrivateKeys = false)
        {
            StringCollection supportedExtensions = withPrivateKeys
                ? SettingsHelper.GetPrivateKeysExtensions()
                : SettingsHelper.GetPublicKeysExtensions();
            List<string> certificatePaths = new List<string>();
            IEnumerable<string> filesInDirectory;

            foreach (string path in locationPaths)
                foreach (string ext in supportedExtensions)
                    if (Directory.Exists(path))
                    {
                        filesInDirectory = Directory.EnumerateFiles(path, ext, SearchOption.AllDirectories);
                        foreach (string filePath in filesInDirectory)
                            if (!certificatePaths.Contains(filePath))
                                certificatePaths.Add(filePath);
                    }

            return certificatePaths;
        }

        public static X509Certificate2Collection GetCertificatesFromPaths(List<string> certificatesPaths)
        {
            X509Certificate2Collection certificates = new X509Certificate2Collection();
            X509Certificate2 cert;

            foreach (string path in certificatesPaths)
            {
                cert = new X509Certificate2(path);
                if (!certificates.Contains(cert))
                    if (IsCertificateTimeValidNow(cert))
                        certificates.Add(cert);
            }

            return certificates;
        }

        public static void MatchCertificatesToRecipients(X509Certificate2Collection certificates,
            ICollection<Recipient> recipients)
        {
            X509Certificate2 currentMatchingCertificate = new X509Certificate2();
            bool certificateForCurrentRecipientFound = false;

            foreach (Recipient recipient in recipients)
            {
                foreach (X509Certificate2 cert in certificates)
                    if (TryMatchToRecipient(cert, recipient))
                    {
                        currentMatchingCertificate = cert;
                        certificateForCurrentRecipientFound = true;
                        break;
                    }

                if (certificateForCurrentRecipientFound)
                {
                    certificates.Remove(currentMatchingCertificate);
                    certificateForCurrentRecipientFound = false;
                }
            }
        }

        public static void MatchCertificatesToRecipients(ICollection<string> certificatesPaths,
            ICollection<Recipient> recipients, bool certsWithPrivateKeys = true)
        {
            string currentMatchingCertificatePath = "";
            bool certificateForCurrentRecipientFound = false;

            foreach (Recipient recipient in recipients)
            {
                foreach (string certPath in certificatesPaths)
                    if (CheckRecipientMatch(certPath, recipient, certsWithPrivateKeys))
                    {
                        currentMatchingCertificatePath = certPath;
                        certificateForCurrentRecipientFound = true;
                        break;
                    }

                if (certificateForCurrentRecipientFound)
                {
                    certificatesPaths.Remove(currentMatchingCertificatePath);
                    certificateForCurrentRecipientFound = false;
                }
            }
        }

        public static RSACryptoServiceProvider ReadPrivateKeyFromPemFile(string pemFileLocation, string password,
            int publicKeySize, bool standardVersion = false)
        {
            RSACryptoServiceProvider rsaCsp = new RSACryptoServiceProvider();
            RSAParameters rsaParameters;

            using (StreamReader reader = new StreamReader(pemFileLocation, SettingsHelper.GetDefaultFileEncoding()))
            {
                try
                {
                    if (standardVersion)
                    {
                        AsymmetricCipherKeyPair keyPair;
                        PemReader pemReader = new PemReader(reader, new Password(password));
                        pemReader.ReadObject(); // reads certificate, I already have it
                        keyPair = (AsymmetricCipherKeyPair) pemReader.ReadObject();
                        rsaParameters = ToRSAParameters((RsaPrivateCrtKeyParameters) keyPair.Private);
                    }
                    else
                    {
                        string wholeFile = reader.ReadToEnd();
                            // quite ugly, but easy, and the file will not be big, so it will not impact the performance much
                        byte[] privateBytesFromBase64 = ExtractPrivateKeyFromString(wholeFile);
                        byte[] decodedPrivate = DecodeKeyWithPassword(privateBytesFromBase64, password);
                        Asn1Object privateKey = Asn1Object.FromByteArray(decodedPrivate);
                        var privStruct = new RsaPrivateKeyStructure((Asn1Sequence) privateKey);
                        rsaParameters = ToRSAParameters(privStruct);
                    }
                }
                catch
                {
                    /*MessageBox.Show(String.Format("Error occured during accessing the private key in file {0}. Make sure the password is correct and try again. If the problem persists, this may mean that the file is corrupted. ", 
                        Path.GetFileName(pemFileLocation)), Languages.Lang.Error, MessageBoxButton.OK, MessageBoxImage.Error);*/
                    return null;
                    /* that takes too long and makes a user think "something's wrong" */
                    /*AsymmetricCipherKeyPair keyPair;
                    RsaKeyPairGenerator g = new RsaKeyPairGenerator();
                    g.Init(new KeyGenerationParameters(new SecureRandom(), publicKeySize));
                    keyPair = g.GenerateKeyPair();
                    rsaParameters = ToRSAParameters((RsaPrivateCrtKeyParameters)keyPair.Private);*/
                }

                rsaCsp.ImportParameters(rsaParameters);
                return rsaCsp;
            }
        }

        public static bool IsStoreAvailable(X509Store store)
        {
            bool available = false;
            try
            {
                store.Open(OpenFlags.OpenExistingOnly);
                available = true;
            }
            catch
            {
                available = false;
            }
            finally
            {
                store.Close();
            }
            return available;
        }

        public static bool TryMatchToRecipient(X509Certificate2 certificate, Recipient recipient)
        {
            if (recipient.IsThatMe(certificate.GetNameInfo(X509NameType.SimpleName, false),
                certificate.GetNameInfo(X509NameType.EmailName, false)))
            {
                recipient.Certificate = certificate;
                return true;
            }
            return false;
        }

        public static bool CheckRecipientMatch(string certificatePath, Recipient recipient,
            bool certWithPrivateKey = true)
        {
            X509Certificate2 currentCertificate;
            bool shouldTryMatch = false;

            if (certWithPrivateKey)
                shouldTryMatch = ContainsPrivateKey(certificatePath, SettingsHelper.UseStandardPrivateKeyFormat());
            else shouldTryMatch = true;

            if (shouldTryMatch)
            {
                currentCertificate = new X509Certificate2(certificatePath);
                if (TryMatchToRecipient(currentCertificate, recipient))
                {
                    recipient.CertificateFilePath = certificatePath;
                    return true;
                }
            }

            return false;
        }

        /* standardVersion == true - checks presence of standard PKCS#1 private key section; 
         * otherwise, checks presence of custom private key section */

        public static bool ContainsPrivateKey(string pemFileLocation, bool standardVersion = false)
        {
            Encoding fileEncoding = SettingsHelper.GetDefaultFileEncoding();
            string fileChunk = "";
            string beginPrivateKey = standardVersion ? BeginStandardPrivateKey : BeginCustomPrivateKey;
            string endPrivateKey = standardVersion ? EndStandardPrivateKey : EndCustomPrivateKey;
            bool beginRsaFound = false;
            int beginSectionPosition = -1;
            int endSectionPosition = -1;
            bool endRsaFound = false;
            bool contentBetweenRsaDelimitersFound = false;

            using (StreamReader tr = new StreamReader(pemFileLocation, fileEncoding))
            {
                fileEncoding = tr.CurrentEncoding;
                while (fileChunk != null)
                {
                    fileChunk = tr.ReadLine();
                    if (fileChunk != null)
                        if (!beginRsaFound)
                        {
                            beginSectionPosition = fileChunk.LastIndexOf(beginPrivateKey,
                                StringComparison.InvariantCulture);
                            if (beginSectionPosition >= 0)
                            {
                                beginRsaFound = true;
                                if (beginSectionPosition + beginPrivateKey.Length < fileChunk.Length)
                                {
                                    endSectionPosition = fileChunk.LastIndexOf(endPrivateKey,
                                        StringComparison.InvariantCulture);
                                    if (endSectionPosition >= 0)
                                    {
                                        endRsaFound = true;
                                        if (endSectionPosition > beginSectionPosition + beginPrivateKey.Length)
                                            contentBetweenRsaDelimitersFound = true;
                                    }
                                }
                            }
                        }
                        else if (!endRsaFound)
                        {
                            endSectionPosition = fileChunk.LastIndexOf(endPrivateKey, StringComparison.InvariantCulture);
                            if (endSectionPosition >= 0)
                            {
                                endRsaFound = true;
                                /*
                                 * if (beginRsaFound == true && endRsaFound == false && endSection JUST FOUND) - it means that if some content 
                                 * was present after the beginSection, then it was an encrypted key, not the endSection, so contentBetweenRsaDelimitersFound 
                                 * is already set to true. If there was no additional content in the same line after beginSection, but there is some content 
                                 * before the endSection (in the same line), then this content is an encrypted key - so set contentBetweenRsaDelimitersFound to true.
                                 */
                                if (endSectionPosition > 0)
                                    contentBetweenRsaDelimitersFound = true;
                            }

                            /* 
                             * if beginSection found and endSection STILL not found, then the content just read is a part of an encrypted key - 
                             * so set contentBetweenRsaDelimitersFound to true 
                             */
                            else contentBetweenRsaDelimitersFound = true;
                        }
                }
            }

            return contentBetweenRsaDelimitersFound;
        }

        private static bool IsCertificateTimeValidNow(X509Certificate2 certificate)
        {
            DateTime now = DateTime.Now;
            return (now > certificate.NotBefore) && (now < certificate.NotAfter);
        }

        #region Private Key Helpers

        private static byte[] ExtractPrivateKeyFromString(string wholeFileAsText)
        {
            int beginPrivateSectionLength = BeginCustomPrivateKey.Length + Environment.NewLine.Length;
            int beginPrivateSectionPosition = wholeFileAsText.LastIndexOf(BeginCustomPrivateKey);
            int endPrivateSectionPosition = wholeFileAsText.LastIndexOf(EndCustomPrivateKey) -
                                            Environment.NewLine.Length;
            string privateKeyBase64 = wholeFileAsText.Substring(beginPrivateSectionPosition + beginPrivateSectionLength,
                endPrivateSectionPosition - (beginPrivateSectionPosition + beginPrivateSectionLength));
            return Convert.FromBase64String(privateKeyBase64);
        }

        private static byte[] DecodeKeyWithPassword(byte[] encodedPrivateKey, string password)
        {
            DigestBase tiger = new TigerDigest();
            byte[] passHash = tiger.GenerateHash(password);
            CipherEngineBase des3 = new TripleDesEngine(passHash, null);
            des3.Init(new CipherData(), false);

            byte[] outputBuffer = new byte[des3.GetOutputSize(encodedPrivateKey.Length)];
            int offset = des3.ProcessBytes(encodedPrivateKey, 0, encodedPrivateKey.Length, outputBuffer, 0);
            des3.DoFinal(outputBuffer, offset);

            return outputBuffer;
        }

        #endregion // end of Private Key Helpers

        #region Bouncy Castle DotNetUtilities Latest Git Version Copied

        public static RSAParameters ToRSAParameters(RsaPrivateCrtKeyParameters privKey)
        {
            RSAParameters rsaParameters = new RSAParameters();
            rsaParameters.Modulus = privKey.Modulus.ToByteArrayUnsigned();
            rsaParameters.Exponent = privKey.PublicExponent.ToByteArrayUnsigned();
            rsaParameters.P = privKey.P.ToByteArrayUnsigned();
            rsaParameters.Q = privKey.Q.ToByteArrayUnsigned();
            rsaParameters.D = ConvertRSAParametersField(privKey.Exponent, rsaParameters.Modulus.Length);
            rsaParameters.DP = ConvertRSAParametersField(privKey.DP, rsaParameters.P.Length);
            rsaParameters.DQ = ConvertRSAParametersField(privKey.DQ, rsaParameters.Q.Length);
            rsaParameters.InverseQ = ConvertRSAParametersField(privKey.QInv, rsaParameters.Q.Length);
            return rsaParameters;
        }

        public static RSAParameters ToRSAParameters(RsaPrivateKeyStructure privStruct)
        {
            RSAParameters rsaParameters = new RSAParameters();
            rsaParameters.Modulus = privStruct.Modulus.ToByteArrayUnsigned();
            rsaParameters.Exponent = privStruct.PublicExponent.ToByteArrayUnsigned();
            rsaParameters.P = privStruct.Prime1.ToByteArrayUnsigned();
            rsaParameters.Q = privStruct.Prime2.ToByteArrayUnsigned();
            rsaParameters.D = ConvertRSAParametersField(privStruct.PrivateExponent, rsaParameters.Modulus.Length);
            rsaParameters.DP = ConvertRSAParametersField(privStruct.Exponent1, rsaParameters.P.Length);
            rsaParameters.DQ = ConvertRSAParametersField(privStruct.Exponent2, rsaParameters.Q.Length);
            rsaParameters.InverseQ = ConvertRSAParametersField(privStruct.Coefficient, rsaParameters.Q.Length);

            return rsaParameters;
        }

        private static byte[] ConvertRSAParametersField(BigInteger n, int size)
        {
            byte[] bs = n.ToByteArrayUnsigned();

            if (bs.Length == size)
                return bs;

            if (bs.Length > size)
                throw new ArgumentException("Specified size too small", "size");

            byte[] padded = new byte[size];
            Array.Copy(bs, 0, padded, size - bs.Length, bs.Length);
            return padded;
        }

        #endregion // end of Bouncy Castle DotNetUtilities Latest Git Version Copied

        #region Message Parts

        private static string ToAvoidInFuture = "To avoid this message in future, ";
        private static string RunWithHigherPriviledges = "run the application with higher priviledges";
        private static string UncheckStoreHint = "uncheck this store in \"Keys->Choose default key localizations\" menu";

        private static string SectionDelimiterMark = "-----";
        private static string BeginSpace = "BEGIN ";
        private static string EndSpace = "END ";
        private static string StandardRsaHeader = "RSA PRIVATE KEY";
        private static string Des3EcbEncryptedRsa = "DES3ECB TIGER ENCRYPTED RSA PRIVATE KEY";

        private static string BeginCustomPrivateKey = SectionDelimiterMark + BeginSpace + Des3EcbEncryptedRsa +
                                                      SectionDelimiterMark;

        private static string EndCustomPrivateKey = SectionDelimiterMark + EndSpace + Des3EcbEncryptedRsa +
                                                    SectionDelimiterMark;

        private static string BeginStandardPrivateKey = SectionDelimiterMark + BeginSpace + StandardRsaHeader +
                                                        SectionDelimiterMark;

        private static string EndStandardPrivateKey = SectionDelimiterMark + EndSpace + StandardRsaHeader +
                                                      SectionDelimiterMark;

        private static string GetStorePresentationStringForError(X509Store store)
        {
            return string.Format("store named {0} in location {1}", store.Name, store.Location);
        }

        private static string GetExceptionMessage(Exception ex)
        {
            return string.Format("Exception message: {0}", ex.Message);
        }

        #endregion // end of Message Parts
    }
}