﻿using System.Collections.Specialized;

namespace ThreeDES.Helpers
{
    public class ExtensionsHelper
    {
        private static string NotSupportedExtensionDescription = "File extension {0} is not supported.";
        public static string ArgumentExceptionDescription = "Files of selected format are not supported.";

        public static string NotImplementedExceptionDescription =
            "Files of selected format will be supported, but this has not been implemented yet.";

        public static bool IsExtensionSupported(string fileExtension, StringCollection extensionList)
        {
            foreach (string filterEntry in extensionList)
                if (filterEntry.Contains(fileExtension))
                    return true;
            return false;
        }

        public static string CreateExtensionErrorMessage(string extension)
        {
            return string.Format(NotSupportedExtensionDescription, extension);
        }
    }
}