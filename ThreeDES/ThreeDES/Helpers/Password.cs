﻿using Org.BouncyCastle.OpenSsl;

namespace ThreeDES.Helpers
{
    public class Password : IPasswordFinder
    {
        private string password;

        public Password(string password)
        {
            this.password = password;
        }

        public char[] GetPassword()
        {
            return password.ToCharArray();
        }
    }
}