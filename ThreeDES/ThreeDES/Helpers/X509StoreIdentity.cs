﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using CommonControls;

namespace ThreeDES.Helpers
{
    public class X509StoreIdentity : ILabeled
    {
        public X509StoreIdentity(string name, string location)
        {
            if (IsStoreNameValid(name) && IsStoreLocationValid(location))
            {
                StoreName = name;
                StoreLocation = location;
            }
        }

        public string StoreName { get; private set; }

        public string StoreLocation { get; private set; }

        public string Label
        {
            get { return StoreName; }
        }

        public static bool IsStoreNameValid(string name)
        {
            return EnumContainsName(typeof(StoreName), name);
        }

        public static bool IsStoreLocationValid(string location)
        {
            return EnumContainsName(typeof(StoreLocation), location);
        }

        private static bool EnumContainsName(Type enumType, string value)
        {
            return Enum.GetNames(enumType).Contains(value, StringComparer.InvariantCultureIgnoreCase);
        }
    }
}