﻿using System.Windows;
using ThreeDES.Helpers;

namespace ThreeDES
{
    /// <summary>
    ///     Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        protected override void OnStartup(StartupEventArgs e)
        {
            SettingsHelper.ActivateCurrentCulture();
            base.OnStartup(e);
        }
    }
}