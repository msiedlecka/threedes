﻿using System.Windows;

namespace ThreeDES.Views
{
    /// <summary>
    ///     Interaction logic for DefaultLocationsPickerView.xaml
    /// </summary>
    public partial class DefaultLocationsPickerView : Window
    {
        public DefaultLocationsPickerView()
        {
            InitializeComponent();
        }
    }
}