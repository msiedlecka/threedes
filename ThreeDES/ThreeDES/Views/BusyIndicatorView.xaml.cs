﻿using System.Windows;

namespace ThreeDES.Views
{
    /// <summary>
    ///     Interaction logic for BusyIndicatorView.xaml
    /// </summary>
    public partial class BusyIndicatorView : Window
    {
        public BusyIndicatorView()
        {
            InitializeComponent();
        }
    }
}