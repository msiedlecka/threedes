﻿using System.Windows;
using ThreeDES.ViewModels;

namespace ThreeDES
{
    /// <summary>
    ///     Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            MainViewModel mvm = new MainViewModel();
            mvm.RequestClose += (obj, args) =>
            {
                Closing -= mvm.OnMainWindowClosing;
                Close();
            };
            Closing += mvm.OnMainWindowClosing;
            DataContext = mvm;
        }
    }
}