﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Security;
using ThreeDES.Helpers;

namespace ThreeDES.Cryptography
{
    public abstract class DigestBase
    {
        protected IDigest digest;
        protected byte[] hash;

        public DigestBase()
        {
            digest = DigestUtilities.GetDigest(DigestName);
            hash = new byte[digest.GetDigestSize()];
        }

        protected abstract string DigestName { get; }

        public byte[] GenerateHash(string text)
        {
            byte[] textBytes = SettingsHelper.GetDefaultFileEncoding().GetBytes(text);
            digest.BlockUpdate(textBytes, 0, textBytes.Length);
            digest.DoFinal(hash, 0);
            digest.Reset();
            return hash;
        }
    }
}