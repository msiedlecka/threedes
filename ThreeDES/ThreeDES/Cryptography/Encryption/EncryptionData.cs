﻿using System.Collections.Generic;
using ThreeDES.Cryptography.Data;

namespace ThreeDES.Cryptography.Encryption
{
    public class EncryptionData : IInputOutputInfo, ICipherDataHolder
    {
        public List<Recipient> Recipients { get; set; }

        public CipherData CipherData { get; set; }

        public string InputPath { get; set; }

        public string OutputPath { get; set; }
    }
}