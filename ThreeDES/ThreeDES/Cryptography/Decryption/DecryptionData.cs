﻿using ThreeDES.Cryptography.Data;

namespace ThreeDES.Cryptography.Decryption
{
    public class DecryptionData : IInputOutputInfo, ICipherDataHolder
    {
        public byte[] InitializationVector { get; set; }

        public Recipient Recipient { get; set; }

        public string PasswordToPrivateKey { get; set; }

        public CipherData CipherData { get; set; }

        public string InputPath { get; set; }

        public string OutputPath { get; set; }

        public void InitWith(EncryptedFileHeader header)
        {
            InitializationVector = header.InitializationVector;
            CipherData = new CipherData(header.Mode, header.SegmentSize);
        }

        public void Clear()
        {
            InputPath = "";
            OutputPath = "";
            InitializationVector = new byte[0];
            Recipient = null;
            PasswordToPrivateKey = "";
            CipherData = null;
        }
    }
}