﻿using System.Security.Cryptography;

namespace ThreeDES.Cryptography
{
    public class CipherData
    {
        public CipherData(CipherMode mode = CipherMode.ECB, int segmentLength = 8)
        {
            Mode = mode;
            SegmentLength = segmentLength;
        }

        public CipherMode Mode { get; set; }

        public int SegmentLength { get; set; }
    }
}