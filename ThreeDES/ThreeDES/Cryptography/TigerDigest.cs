﻿namespace ThreeDES.Cryptography
{
    public class TigerDigest : DigestBase
    {
        protected override string DigestName
        {
            get { return "Tiger"; }
        }
    }
}