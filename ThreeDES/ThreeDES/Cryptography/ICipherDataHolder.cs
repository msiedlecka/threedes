﻿namespace ThreeDES.Cryptography
{
    public interface ICipherDataHolder
    {
        CipherData CipherData { get; set; }
    }
}