﻿namespace ThreeDES.Cryptography
{
    public interface IInputOutputInfo
    {
        string InputPath { get; set; }

        string OutputPath { get; set; }
    }
}