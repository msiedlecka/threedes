﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Xml.Schema;
using System.Xml.Serialization;
using CommonControls;
using ExtensionMethods;
using ThreeDES.Cryptography.Encryption;
using ThreeDES.Helpers;

namespace ThreeDES.Cryptography.Data
{
    [Serializable]
    [XmlRoot("encryptedFileHeader", Namespace = "", IsNullable = false)]
    public class EncryptedFileHeader : PropertyChangedNotifier
    {
        public EncryptedFileHeader()
        {
            AlgorithmName = SettingsHelper.GetXmlFixedAlgorithmName();
        }

        public EncryptedFileHeader(EncryptionData data, byte[] initVector) : this()
        {
            Mode = data.CipherData.Mode;
            SegmentSize = data.CipherData.SegmentLength;
            ApprovedRecipients = data.Recipients;
            InitializationVector = initVector;
        }

        #region Fields

        private string algorithmName;
        private CipherMode mode;
        private int segmentSize;
        private byte[] initializationVector;
        private List<Recipient> approvedRecipients;

        #endregion // end of Fields

        #region Properties

        [XmlElement("algorithmName", Form = XmlSchemaForm.Unqualified)]
        public string AlgorithmName
        {
            get { return algorithmName; }
            set
            {
                algorithmName = value;
                RaisePropertyChanged("AlgorithmName");
            }
        }

        [XmlElement("cipherMode", Form = XmlSchemaForm.Unqualified)]
        public CipherMode Mode
        {
            get { return mode; }
            set
            {
                mode = value;
                RaisePropertyChanged("Mode");
            }
        }

        [XmlElement("segmentSize", Form = XmlSchemaForm.Unqualified)]
        public int SegmentSize
        {
            get { return segmentSize; }
            set
            {
                segmentSize = value;
                RaisePropertyChanged("SegmentSize");
            }
        }

        [XmlElement("iv", Form = XmlSchemaForm.Unqualified)]
        public byte[] InitializationVector
        {
            get { return initializationVector; }
            set
            {
                initializationVector = value;
                RaisePropertyChanged("InitializeVector");
            }
        }

        [XmlArray("approvedRecipients", Form = XmlSchemaForm.Unqualified)]
        [XmlArrayItem("recipient", Form = XmlSchemaForm.Unqualified, IsNullable = false)]
        public List<Recipient> ApprovedRecipients
        {
            get { return approvedRecipients; }
            set
            {
                approvedRecipients = value;
                RaisePropertyChanged("ApprovedRecipients");
            }
        }

        #endregion // end of Properties

        #region Methods

        public bool ShouldSerializeInitializationVector()
        {
            return CipherEngineBase.IsIVUsedForMode(Mode);
        }

        public bool ShouldSerializeSegmentSize()
        {
            return CipherEngineBase.IsSegmentSizeUsedForMode(Mode);
        }

        public bool IsValid()
        {
            CipherEngineBase des3 = new TripleDesEngine();
            return SettingsHelper.GetXmlFixedAlgorithmName().Equals(AlgorithmName) &&
                   ApprovedRecipients != null && ApprovedRecipients.Count > 0 &&
                   (CipherEngineBase.IsIVUsedForMode(Mode)
                       ? InitializationVector != null && InitializationVector.Length == des3.BlockSize/8
                       : true) &&
                   (CipherEngineBase.IsSegmentSizeUsedForMode(Mode)
                       ? SegmentSize.InClosedInterval(des3.MinSegmentLength, des3.MaxSegmentLength)
                       : true);
        }

        #endregion // end of Methods
    }
}