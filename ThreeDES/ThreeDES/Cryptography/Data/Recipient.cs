﻿using System;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Windows;
using System.Xml.Schema;
using System.Xml.Serialization;
using CommonControls;
using ThreeDES.Helpers;
using ThreeDES.Languages;

namespace ThreeDES.Cryptography.Data
{
    [Serializable]
    [XmlType(TypeName = "recipient")]
    public class Recipient : PropertyChangedNotifier, ILabeled
    {
        public void EncryptPlainSessionKey(byte[] plainKey)
        {
            RSACryptoServiceProvider rsa = Certificate.PublicKey.Key as RSACryptoServiceProvider;
            if (rsa != null)
                EncryptedSessionKey = rsa.Encrypt(plainKey, false);
        }

        public byte[] DecryptSessionKey(string password)
        {
            RSACryptoServiceProvider rsa;

            if (Certificate != null)
            {
                if (CertificateFilePath == null || CertificateFilePath == "")
                {
                    MessageBox.Show(string.Format("Error: Cannot retrieve private key for recipient: {0}. ", Label),
                        Lang.Error, MessageBoxButton.OK, MessageBoxImage.Error);
                    return null;
                }
                rsa = CertificateHelper.ReadPrivateKeyFromPemFile(CertificateFilePath, password,
                    Certificate.PublicKey.Key.KeySize, SettingsHelper.UseStandardPrivateKeyFormat());

                CipherEngineBase des3 = new TripleDesEngine();
                if (rsa != null)
                    try
                    {
                        return rsa.Decrypt(EncryptedSessionKey, false);
                    }
                    catch (CryptographicException ce)
                    {
                        /*MessageBox.Show(String.Format("Error while decrypting session key for {0}: the private key doesn't match the encrypted data ({1})",
                            Label, ce.Message), Languages.Lang.Error, MessageBoxButton.OK, MessageBoxImage.Error);*/
                        return des3.GenerateKey();
                    }
                return des3.GenerateKey();
            }
            MessageBox.Show(
                string.Format(
                    "Error: Cannot retrieve private key for recipient: {0}, because no matching private key file was found.",
                    Label),
                Lang.Error, MessageBoxButton.OK, MessageBoxImage.Error);
            return null;
        }

        public void ForgetPrivateKey()
        {
            if (Certificate != null)
                Certificate.PrivateKey = null;
        }

        public bool ShouldSerializeEmail()
        {
            return false;
        }

        public bool IsThatMe(string name, string email)
        {
            return Name.Equals(name) && ((Email != null && Email != "") ? email.Equals(Email) : true);
        }

        public override bool Equals(object obj)
        {
            Recipient compared = obj as Recipient;
            if (compared == null)
                return false;
            if (Certificate != null)
                if (EncryptedSessionKey != null)
                    return Certificate.Equals(compared.Certificate) &&
                           EncryptedSessionKey.Equals(compared.EncryptedSessionKey);
                else
                    return Certificate.Equals(compared.Certificate);
            if (CertificateFilePath != null)
                return
                    FileDialogHelper.ToAbsolutePath(CertificateFilePath)
                        .Equals(FileDialogHelper.ToAbsolutePath(compared.CertificateFilePath));
            return false;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #region Fields

        private string name;
        private string email;
        private byte[] encryptedSessionKey;
        private X509Certificate2 certificate;
        private string certificateFilePath;

        #endregion // end of Fields

        #region Properties

        [XmlElement("name", Form = XmlSchemaForm.Unqualified)]
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                RaisePropertyChanged("Name");
            }
        }

        [XmlIgnore]
        public X509Certificate2 Certificate
        {
            get { return certificate; }
            set
            {
                certificate = value;
                Name = certificate.GetNameInfo(X509NameType.SimpleName, false);
                Email = certificate.GetNameInfo(X509NameType.EmailName, false);
                RaisePropertyChanged("Certificate");
            }
        }

        [XmlIgnore]
        public string CertificateFilePath
        {
            get { return certificateFilePath; }
            set
            {
                certificateFilePath = value;
                RaisePropertyChanged("CertificateFilePath");
            }
        }

        [XmlElement("email", Form = XmlSchemaForm.Unqualified)]
        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                RaisePropertyChanged("Email");
            }
        }

        [XmlElement("sessionKey", DataType = "base64Binary", Form = XmlSchemaForm.Unqualified)]
        public byte[] EncryptedSessionKey
        {
            get { return encryptedSessionKey; }
            set
            {
                encryptedSessionKey = value;
                RaisePropertyChanged("EncryptedSessionKey");
            }
        }

        /**
         * Formats:
         * Name (Email)     -- when both specified
         * Name             -- when only name specified (should be always present as a mandatory field in an x509 certificate)
         * Mail             -- when only email specified
         * 
         * EDIT: just name
         */

        [XmlIgnore]
        public string Label
        {
            get
            {
                /* possible formats */
                /*string hasBoth = "{0} ({1})";
                string hasOnlyName = "{0}";
                string hasOnlyEmail = "{1}";
                string chosenFormat;

                bool hasName = false, hasEmail = false;

                if (Name != null && Name != "") hasName = true;
                if (Email != null && Email != "") hasEmail = true;

                if (hasName && hasEmail) chosenFormat = hasBoth;
                else if (hasName) chosenFormat = hasOnlyName;
                else chosenFormat = hasOnlyEmail;

                return String.Format(chosenFormat, Name, Email);*/

                return Name;
            }
        }

        #endregion // end of Properties

        #region Constructors

        public Recipient()
        {
            Name = "";
            Email = "";
        }

        /* for certs from folders */

        public Recipient(string certificateFilePath)
        {
            CertificateFilePath = certificateFilePath;
            Certificate = new X509Certificate2(CertificateFilePath);
        }

        /* for certs from system savedStores */

        public Recipient(X509Certificate2 certificate)
        {
            Certificate = certificate;
        }

        #endregion // end of Constructors
    }
}