﻿using System;
using System.Security.Cryptography;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Prng;
using Org.BouncyCastle.Security;

namespace ThreeDES.Cryptography
{
    public abstract class CipherEngineBase
    {
        /* hardcoded, because it's algorithm-specific and fixed, so why to bother reading from settings file */
        private static readonly string PKCS7_PADDING_NAME = "PKCS7Padding";

        protected static SecureRandom random;
        protected static object randomSynchronizer;
        protected static int randomUsageCounter;
        protected IBufferedCipher bufferedCipher;
        protected ICipherParameters cipherParameters;

        protected CipherKeyGenerator keyGenerator;

        private int WRONG_USAGE_FLAG = -1;

        private void InitializeKeyGeneration()
        {
            keyGenerator = GetKeyGenerator();
            lock (randomSynchronizer)
            {
                keyGenerator.Init(new KeyGenerationParameters(random, KeySize));
                UpdateRandom();
            }
        }

        #region Public

        #region Constructors and initializers

        static CipherEngineBase()
        {
            random = new SecureRandom(new DigestRandomGenerator(DigestUtilities.GetDigest("SHA-512")));
            randomSynchronizer = new object();
            randomUsageCounter = 0;
            InitializeRandom();
        }

        public CipherEngineBase()
        {
            InitializeKeyGeneration();
        }

        public CipherEngineBase(byte[] key, byte[] iv)
            : this()
        {
            Key = key;
            IV = iv;
        }

        #endregion // end of Constructors and initializers

        public abstract string AlgorithmName { get; }

        public abstract int MinSegmentLength { get; }

        public abstract int MaxSegmentLength { get; }

        public abstract int KeySize { get; }

        public abstract int BlockSize { get; }

        public byte[] Key { get; protected set; }

        public byte[] IV { get; protected set; }

        public static bool IsSegmentSizeUsedForMode(CipherMode mode)
        {
            return CipherMode.CFB.Equals(mode) || CipherMode.OFB.Equals(mode);
        }

        public static bool IsIVUsedForMode(CipherMode mode)
        {
            return !CipherMode.ECB.Equals(mode);
        }

        public static byte[] GetRandomBytes(int length)
        {
            byte[] randomBytes = new byte[length];
            lock (randomSynchronizer)
            {
                random.NextBytes(randomBytes, 0, length);
                UpdateRandom();
            }
            return randomBytes;
        }

        public byte[] GenerateIV()
        {
            return GetRandomBytes(BlockSize/8);
        }

        public byte[] GenerateKey()
        {
            byte[] key;
            lock (randomSynchronizer)
            {
                key = keyGenerator.GenerateKey();
                UpdateRandom();
                return key;
            }
        }

        public void Init(ICipherDataHolder holder, bool encrypt)
        {
            Init(holder.CipherData, encrypt);
        }

        public void Init(CipherData data, bool encrypt)
        {
            bufferedCipher = CipherUtilities.GetCipher(TransformationName(AlgorithmName, data));

            KeyParameter keyParameter = ParameterUtilities.CreateKeyParameter(AlgorithmName, Key);
            if (IsIVUsedForMode(data.Mode))
                cipherParameters = new ParametersWithIV(keyParameter, IV);
            else cipherParameters = keyParameter;
            bufferedCipher.Init(encrypt, cipherParameters);
        }

        public void Reset()
        {
            if (bufferedCipher != null)
                bufferedCipher.Reset();
        }

        public int ProcessBytes(byte[] input, int inOff, int length, byte[] output, int outOff)
        {
            if (bufferedCipher != null)
                return bufferedCipher.ProcessBytes(input, inOff, length, output, outOff);
            return WRONG_USAGE_FLAG;
        }

        public byte[] DoFinal()
        {
            if (bufferedCipher != null)
                return bufferedCipher.DoFinal();
            return null;
        }

        public void DoFinal(byte[] outputBuffer, int outputOffset)
        {
            if (bufferedCipher != null)
                bufferedCipher.DoFinal(outputBuffer, outputOffset);
        }

        public int GetOutputSize(int inputLength)
        {
            return bufferedCipher.GetOutputSize(inputLength);
        }

        #endregion // end of Public

        #region Protected

        protected static void InitializeRandom()
        {
            random.SetSeed(new ThreadedSeedGenerator().GenerateSeed(20, false));
        }

        /* may be called ONLY within lock(randomSynchronizer) scope! */

        protected static void UpdateRandom()
        {
            randomUsageCounter++;
            if (randomUsageCounter%200 == 0)
                InitializeRandom();
        }

        protected static string TransformationName(string algorithmName, CipherData data)
        {
            return string.Format("{0}/{1}/{2}", algorithmName, ModeWithOptionalSegment(data), Pkcs7PaddingName);
        }

        protected static string Pkcs7PaddingName
        {
            get { return PKCS7_PADDING_NAME; }
        }

        protected static string ModeToString(CipherMode mode)
        {
            return Enum.GetName(typeof(CipherMode), mode);
        }

        protected static string ModeWithOptionalSegment(CipherData data)
        {
            return string.Format("{0}{1}", ModeToString(data.Mode),
                IsSegmentSizeUsedForMode(data.Mode) ? data.SegmentLength.ToString() : "");
        }

        protected virtual CipherKeyGenerator GetKeyGenerator()
        {
            return new CipherKeyGenerator();
        }

        #endregion // end of Protected
    }
}