﻿using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Generators;

namespace ThreeDES.Cryptography
{
    public class TripleDesEngine : CipherEngineBase
    {
        private static readonly string DES3_ALGORITHM_NAME = "DESede";
        private static readonly int DES3_BLOCK_SIZE = 64;
        private static readonly int DES3_KEY_SIZE = 192;
        private static readonly int MIN_DES3_SEGMENT_LENGTH = 1;
        private static readonly int MAX_DES3_SEGMENT_LENGTH = DES3_BLOCK_SIZE;
        /*
         * should be used ONLY when the object is created just to generate the key and the initialization vector
         */

        public TripleDesEngine()
        {
        }

        public TripleDesEngine(byte[] key, byte[] iv) : base(key, iv)
        {
        }

        public override string AlgorithmName
        {
            get { return DES3_ALGORITHM_NAME; }
        }

        public override int MinSegmentLength
        {
            get { return MIN_DES3_SEGMENT_LENGTH; }
        }

        public override int MaxSegmentLength
        {
            get { return MAX_DES3_SEGMENT_LENGTH; }
        }

        public override int KeySize
        {
            get { return DES3_KEY_SIZE; }
        }

        public override int BlockSize
        {
            get { return DES3_BLOCK_SIZE; }
        }

        protected override CipherKeyGenerator GetKeyGenerator()
        {
            return new DesEdeKeyGenerator();
        }
    }
}